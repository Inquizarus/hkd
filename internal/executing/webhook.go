package executing

import (
	"fmt"
	"os/exec"

	"gitlab.com/inquizarus/hkd/pkg/model"
)

// Webhook executes passed hook and waits or not depending on wait
func Webhook(wh model.Webhook, payload model.Payload) (string, error) {
	cmdPath, err := exec.LookPath(wh.ExecuteCommand)
	if nil != err {
		return "", fmt.Errorf("could not execute webhook with identifier %s, got error \"%s\"", wh.Identifier, err)
	}
	payloadArgs := wh.FilterOnAllowedArguments(payload.GetArguments())
	plargStrings := []string{}
	for k, v := range payloadArgs {
		plargStrings = append(plargStrings, fmt.Sprintf("%s=%s", k, v))
	}

	cmd := exec.Command(cmdPath, append(wh.CommandArgs, plargStrings...)...)
	if "" != wh.WorkingDirectory {
		cmd.Dir = wh.WorkingDirectory
	}
	f := func() (string, error) {
		out, err := cmd.CombinedOutput()
		if err != nil {
			return "", fmt.Errorf("could not execute webhook with identifier %s, got error \"%s\"", wh.Identifier, err)
		}
		return string(out), nil
	}
	if false == payload.ShouldWaitForExecution() {
		go f()
		return "", nil
	}
	return f()
}
