package printing

import (
	"encoding/json"
	"fmt"
	"io"
	"reflect"
	"strings"

	"gitlab.com/inquizarus/hkd/pkg/model"
)

// Printer for handling output
type Printer interface {
	PrintToken(model.Token) error
	PrintTokens([]model.Token) error
	PrintText(string) error
	PrintWebhook(model.Webhook) error
	PrintWebhooks([]model.Webhook) error
}

// DefaultPrinter implementation of Printer interface
type DefaultPrinter struct {
	w io.Writer
}

// PrintToken implementation for DefaultPrinter
func (dp *DefaultPrinter) PrintToken(token model.Token) error {
	fmt.Fprintln(dp.w, "Name AllowedOrigins")
	fmt.Fprintf(dp.w, "%s %s\n", token.Name, token.AllowedOrigins)
	return nil
}

// PrintTokens implementation for DefaultPrinter
func (dp *DefaultPrinter) PrintTokens(tokens []model.Token) error {
	headersDone := false
	headers := []string{}
	rows := [][]string{}
	for _, token := range tokens {
		val := reflect.ValueOf(&token).Elem()
		row := []string{}
		for i := 0; i < val.NumField(); i++ {
			valueField := val.Field(i)
			typeField := val.Type().Field(i)
			//tag := typeField.Tag
			row = append(row, valueField.String())
			if true != headersDone {
				headers = append(headers, typeField.Name)
			}
		}
		if true != headersDone {
			headersDone = true
		}
		rows = append(rows, row)
	}
	fmt.Fprintln(dp.w, strings.Join(headers, " "))
	for _, row := range rows {
		fmt.Fprintln(dp.w, strings.Join(row, " "))
	}
	return nil
}

// PrintText implementation for DefaultPrinter
func (dp *DefaultPrinter) PrintText(txt string) error {
	_, err := fmt.Fprintln(dp.w, txt)
	return err
}

// PrintWebhooks implementation for DefaultPrinter
func (dp *DefaultPrinter) PrintWebhooks(hooks []model.Webhook) (err error) {
	headersDone := false
	headers := []string{}
	rows := [][]string{}
	for _, hook := range hooks {
		val := reflect.ValueOf(&hook).Elem()
		row := []string{}
		for i := 0; i < val.NumField(); i++ {
			valueField := val.Field(i)
			typeField := val.Type().Field(i)
			//tag := typeField.Tag
			row = append(row, valueField.String())
			if true != headersDone {
				headers = append(headers, typeField.Name)
			}
		}
		if true != headersDone {
			headersDone = true
		}
		rows = append(rows, row)
	}
	fmt.Fprintln(dp.w, strings.Join(headers, " "))
	for _, row := range rows {
		fmt.Fprintln(dp.w, strings.Join(row, " "))
	}
	return err
}

// PrintWebhook implementation for DefaultPrinter
func (dp *DefaultPrinter) PrintWebhook(hook model.Webhook) (err error) {
	headersDone := false
	headers := []string{}
	rows := [][]string{}

	val := reflect.ValueOf(&hook).Elem()
	row := []string{}
	for i := 0; i < val.NumField(); i++ {
		valueField := val.Field(i)
		typeField := val.Type().Field(i)
		//tag := typeField.Tag
		row = append(row, valueField.String())
		if true != headersDone {
			headers = append(headers, typeField.Name)
		}
	}
	if true != headersDone {
		headersDone = true
	}
	rows = append(rows, row)

	fmt.Fprintln(dp.w, strings.Join(headers, " "))
	for _, row := range rows {
		fmt.Fprintln(dp.w, strings.Join(row, " "))
	}
	return err
}

// MakeDefaultPrinter returns configured printer for given writer
func MakeDefaultPrinter(w io.Writer) Printer {
	return &DefaultPrinter{
		w: w,
	}
}

// NullPrinter implementation of Printer interface
type NullPrinter struct{}

// PrintToken implementation for NullPrinter
func (dp *NullPrinter) PrintToken(token model.Token) (err error) {
	return nil
}

// PrintTokens implementation for NullPrinter
func (dp *NullPrinter) PrintTokens(tokens []model.Token) (err error) {
	return nil
}

// PrintText implementation for NullPrinter
func (dp *NullPrinter) PrintText(txt string) error {
	return nil
}

// PrintWebhooks implementation for NullPrinter
func (dp *NullPrinter) PrintWebhooks(hooks []model.Webhook) (err error) {
	return nil
}

// PrintWebhook implementation for NullPrinter
func (dp *NullPrinter) PrintWebhook(hook model.Webhook) (err error) {
	return nil
}

// JSONPrinter implementation of Printer interface
type JSONPrinter struct {
	w io.Writer
}

// PrintToken implementation for JSONPrinter
func (dp *JSONPrinter) PrintToken(token model.Token) (err error) {
	b, err := json.Marshal(token)
	if nil == err {
		fmt.Fprint(dp.w, string(b))
	}
	return err
}

// PrintTokens implementation for JSONPrinter
func (dp *JSONPrinter) PrintTokens(tokens []model.Token) (err error) {
	b, err := json.Marshal(tokens)
	if nil == err {
		fmt.Fprint(dp.w, string(b))
	}
	return err
}

// PrintText implementation for JSONPrinter
func (dp *JSONPrinter) PrintText(txt string) error {
	_, err := fmt.Fprintln(dp.w, txt)
	return err
}

// PrintWebhooks implementation for JSONPrinter
func (dp *JSONPrinter) PrintWebhooks(hooks []model.Webhook) (err error) {
	b, err := json.Marshal(hooks)
	if nil == err {
		fmt.Fprint(dp.w, string(b))
	}
	return err
}

// PrintWebhook implementation for JSONPrinter
func (dp *JSONPrinter) PrintWebhook(hook model.Webhook) (err error) {
	b, err := json.Marshal(hook)
	if nil == err {
		fmt.Fprint(dp.w, string(b))
	}
	return err
}

// MakeJSONPrinter returns configured printer for given writer
func MakeJSONPrinter(w io.Writer) Printer {
	return &JSONPrinter{
		w: w,
	}
}
