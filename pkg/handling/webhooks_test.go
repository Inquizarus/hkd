package handling_test

import (
	"bytes"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/inquizarus/hkd/pkg/config"
	"gitlab.com/inquizarus/hkd/pkg/handling"
	"gitlab.com/inquizarus/hkd/pkg/mocking"
	"gitlab.com/inquizarus/hkd/pkg/vcs"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestThatWebhookHandlerHaveCorrectPath(t *testing.T) {
	h := handling.MakeTriggerWebhookHandler(&config.NullHkdConfig{})
	expected := "/webhook/{identifier}"
	if expected != h.GetPath() {
		t.Errorf("incorrect path for webhook handler, got \"%s\" but expected \"%s\"", h.GetPath(), expected)
	}
}

func TestThatWebhooksHandlerHaveCorrectName(t *testing.T) {
	h := handling.MakeTriggerWebhookHandler(&config.NullHkdConfig{})
	expected := "webhook-handler"
	if expected != h.GetName() {
		t.Errorf("incorrect name for webhook handler, got \"%s\" but expected \"%s\"", h.GetName(), expected)
	}
}

func TestThatWebhooksHandlerSetJsonContent(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	cfg := b.Build()
	t.Log("ensure that application/json is set as content type for response")
	h := handling.MakeTriggerWebhookHandler(cfg)
	r := httptest.NewRequest("POST", mocking.ExistingWebhookURL, bytes.NewReader([]byte("")))
	w := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := "application/json"
	actual := w.Header().Get("content-type")
	if expected != actual {
		t.Errorf("incorrect content-type, got \"%s\" but expected \"%s\"", actual, expected)
	}
}

func TestWebhookNotFound(t *testing.T) {
	t.Log("test so that a 404 response is returned if webhook does not exist")
	b := config.StandardHkdConfigBuilder{}
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.NonExistingWebhookURL, bytes.NewReader([]byte("{}")))
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusNotFound
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for request, got \"%d\" but expected \"%d\"", actual, expected)
	}
}

func TestThatItCanExecuteGitlabWebhook(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.ExistingWebhookURL, bytes.NewReader([]byte(`{"wait-for-execution":true}`)))
	r.Header.Set(vcs.GitlabDeliveryHeader, "Push")
	r.Header.Set(vcs.GitlabTokenHeader, mocking.EncodedTokenIdentifier)
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusOK
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for generic request that successfully executed a webhook, got \"%d\" but expected \"%d\"", actual, expected)
	}
	expectedBody := `{"status":200,"message":"successfully executed webhook","output":"\"testing with echo\"\n"}`
	actualBody := w.Body.String()
	if expectedBody != actualBody {
		t.Errorf("incorrect body for generic request that successfully executed a webhook, got %s but wanted %s", actualBody, expectedBody)
	}
}

func TestThatBadGitlabTokenResultsInUnAuthorizedResponse(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.ExistingWebhookURL, bytes.NewReader([]byte(`{"wait-for-execution":true}`)))
	r.Header.Set(vcs.GitlabDeliveryHeader, "Push")
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusUnauthorized
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for generic request that contains bad token, got \"%d\" but expected \"%d\"", actual, expected)
	}
	expectedBody := `{"status":401,"message":"invalid authorization token provided"}`
	actualBody := w.Body.String()
	if expectedBody != actualBody {
		t.Errorf("incorrect body for generic request that contained bad token, got %s but wanted %s", actualBody, expectedBody)
	}
}

func TestThatGithubRequestReturnsNotImplemented(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.ExistingWebhookURL, bytes.NewReader([]byte("{}")))
	r.Header.Set(vcs.GithubDeliveryHeader, "something")
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusNotImplemented
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for github request, got \"%d\" but expected \"%d\"", actual, expected)
	}
}

func TestThatItCanExecuteGiteaWebhook(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.ExistingWebhookURL, bytes.NewReader(
		[]byte(fmt.Sprintf(`{"secret": "%s", "wait-for-execution":true}`, mocking.EncodedTokenIdentifier)),
	))
	r.Header.Set(vcs.GiteaDeliveryHeader, "Push")
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusOK
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for generic request that successfully executed a webhook, got \"%d\" but expected \"%d\"", actual, expected)
	}
	expectedBody := `{"status":200,"message":"successfully executed webhook","output":"\"testing with echo\"\n"}`
	actualBody := w.Body.String()
	if expectedBody != actualBody {
		t.Errorf("incorrect body for generic request that successfully executed a webhook, got %s but wanted %s", actualBody, expectedBody)
	}
}

func TestThatBadGiteaTokenResultsInUnAuthorizedResponse(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.ExistingWebhookURL, bytes.NewReader(
		[]byte(fmt.Sprintf(`{"secret": "%s", "wait-for-execution":true}`, mocking.EncodedInvalidTokenIdentifier)),
	))
	r.Header.Set(vcs.GiteaDeliveryHeader, "Push")
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusUnauthorized
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for generic request that contains bad token, got \"%d\" but expected \"%d\"", actual, expected)
	}
	expectedBody := `{"status":401,"message":"invalid authorization token provided"}`
	actualBody := w.Body.String()
	if expectedBody != actualBody {
		t.Errorf("incorrect body for generic request that contained bad token, got %s but wanted %s", actualBody, expectedBody)
	}
}

func TestThatItCanExecuteGenericWebhook(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.ExistingWebhookURL, bytes.NewReader([]byte(`{"wait-for-execution":true}`)))
	r.Header.Set("Authorization", fmt.Sprintf("Bearer %s", mocking.EncodedTokenIdentifier))
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusOK
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for generic request that successfully executed a webhook, got \"%d\" but expected \"%d\"", actual, expected)
	}
	expectedBody := `{"status":200,"message":"successfully executed webhook","output":"\"testing with echo\"\n"}`
	actualBody := w.Body.String()
	if expectedBody != actualBody {
		t.Errorf("incorrect body for generic request that successfully executed a webhook, got %s but wanted %s", actualBody, expectedBody)
	}
}

func TestThatMalformedPayloadIsHandled(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.ExistingWebhookURL, bytes.NewReader([]byte(`#¤&#%¤`)))
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusBadRequest
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for request with malformed payload, got \"%d\" but expected \"%d\"", actual, expected)
	}
	expectedBody := `{"status":400,"message":"malformed payload in request"}`
	actualBody := w.Body.String()
	if expectedBody != actualBody {
		t.Errorf("incorrect body for request with malformed payload, got %s but wanted %s", actualBody, expectedBody)
	}
}

func TestThatFailedWebhookExecutionIsHandled(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.ExistingFailingWebhookURL, bytes.NewReader([]byte(`{}`)))
	r.Header.Set("Authorization", fmt.Sprintf("Bearer %s", mocking.EncodedTokenIdentifier))
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusInternalServerError
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for request where webhook execution failed, got \"%d\" but expected \"%d\"", actual, expected)
	}
	expectedBody := `{"status":500,"message":"an internal error occured"}`
	actualBody := w.Body.String()
	if expectedBody != actualBody {
		t.Errorf("incorrect body for request where webhook execution failed, got %s but wanted %s", actualBody, expectedBody)
	}
}

func TestThatBadTokenResultsInUnAuthorizedResponse(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	cfg := b.Build()
	r := httptest.NewRequest("POST", mocking.ExistingWebhookURL, bytes.NewReader([]byte(`{"wait-for-execution":true}`)))
	w := httptest.NewRecorder()
	h := handling.MakeTriggerWebhookHandler(cfg)
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusUnauthorized
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect status code for generic request that contains bad token, got \"%d\" but expected \"%d\"", actual, expected)
	}
	expectedBody := `{"status":401,"message":"invalid authorization token provided"}`
	actualBody := w.Body.String()
	if expectedBody != actualBody {
		t.Errorf("incorrect body for generic request that contained bad token, got %s but wanted %s", actualBody, expectedBody)
	}
}
