package middleware

import (
	"encoding/base64"
	"fmt"
	"gitlab.com/inquizarus/hkd/pkg/logging"
	"gitlab.com/inquizarus/hkd/pkg/storage/token"
	"gitlab.com/inquizarus/rest"
	"net/http"
)

// WithGitlabAuthHeaderValidation ensures that the incoming request is coming from gitlab and
// that it contains all the needed information
func WithGitlabAuthHeaderValidation(tr token.Reader, l logging.Logger) rest.Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			gitlabToken := r.Header.Get("X-Gitlab-Token")
			l.Debug(fmt.Sprintf(`incoming hook trigger from gitlab with token "%s"`, gitlabToken))
			if "" == gitlabToken {
				l.Info(fmt.Sprintf("empty X-Gitlab-Token provided"))
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
			decoded, err := base64.StdEncoding.DecodeString(gitlabToken)
			if nil != err {
				w.WriteHeader(http.StatusBadRequest)
				l.Info(fmt.Sprintf(`invalid X-Gitlab-Token "%s" provided`, gitlabToken))
				l.Debug(fmt.Sprintf(`error that caused the token to be invalid was "%s"`, err.Error()))
				return
			}
			_, err = tr.Fetch(string(decoded))
			if nil != err {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
			f(w, r)
		}
	}
}
