package middleware

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/inquizarus/hkd/pkg/logging"
	"net/http"
	"strings"

	b64 "encoding/base64"

	"gitlab.com/inquizarus/hkd/pkg/storage/token"
	"gitlab.com/inquizarus/rest"
)

// WithTokenValidation validates a token passed in headers sent in request
func WithTokenValidation(tr token.Reader, l logging.Logger) rest.Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			auth := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
			if len(auth) != 2 || auth[0] != "Bearer" {
				l.Debug("no authorization header in request")
				writeUnauthorizedResponse(w, "invalid Authorization header provided")
				return
			}
			tb, err := b64.StdEncoding.DecodeString(auth[1])
			if nil != err {
				l.Debug(errors.Wrap(err, "could not decode token properly").Error())
				writeUnauthorizedResponse(w, "invalid token provided")
				return
			}
			_, err = tr.Fetch(string(tb))
			if err != nil {
				l.Error(errors.Wrap(err, "token could not be fetched").Error())
				writeUnauthorizedResponse(w, "void token provided")
				return
			}
			f(w, r)
		}
	}
}

// WithStandaloneTokenValidation ...
func WithStandaloneTokenValidation(tr token.Reader) rest.Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			auth := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
			if len(auth) != 2 || auth[0] != "Bearer" {
				writeUnauthorizedResponse(w, "invalid Authorization header provided")
				return
			}

			tb, err := b64.StdEncoding.DecodeString(auth[1])
			if err != nil {
				writeUnauthorizedResponse(w, " token in Authorization header could not be decoded")
				return
			}

			_, err = tr.Fetch(string(tb))
			if err != nil {
				writeUnauthorizedResponse(w, "invalid token provided")
				return
			}

			f(w, r)
		}
	}
}

func writeUnauthorizedResponse(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusUnauthorized)
	w.Write([]byte(buildBodyString(msg)))
}

func buildBodyString(msg string) string {
	return fmt.Sprintf("{\"message\":\"%s\"}", msg)
}
