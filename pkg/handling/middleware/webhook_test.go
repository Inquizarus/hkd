package middleware_test

import (
	"bytes"
	"encoding/base64"
	"github.com/gorilla/mux"
	"gitlab.com/inquizarus/hkd/pkg/handling/middleware"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"gitlab.com/inquizarus/hkd/pkg/storage/hook"
	"net/http"
	"net/http/httptest"
	"testing"
)

var validWebhookIdentifier = "valid"
var encodedValidWebhookIdentifier = base64.StdEncoding.EncodeToString([]byte(validWebhookIdentifier))

var invalidWebhookIdentifier = "invalidvalid"
var encodedInvalidWebhookIdentifier = base64.StdEncoding.EncodeToString([]byte(invalidWebhookIdentifier))

var mockHookStorage = hook.MockReadWriter{
	FetchResults: map[string]model.Webhook{
		validWebhookIdentifier: model.Webhook{
			Identifier: validWebhookIdentifier,
		},
	},
}

func TestWebhookBadRequest(t *testing.T) {
	t.Log("test so that the middleware returns bad request if no identifier have been provided in the request")
	mw := middleware.WithCheckIfWebhookExists(&mockHookStorage)
	r := httptest.NewRequest("POST", "/", bytes.NewBuffer([]byte("")))
	w := httptest.NewRecorder()
	mw(func(w http.ResponseWriter, r *http.Request) {})(w, r)
	if http.StatusBadRequest != w.Code {
		t.Errorf("wrong status code set when webhook identifier missing, got %d but wanted %d", w.Code, http.StatusBadRequest)
	}
}

func TestWebhookNotExist404(t *testing.T) {
	t.Log("test so that the middleware check the storage for the requested webhook and return 404 if it does not exist")
	mw := middleware.WithCheckIfWebhookExists(&mockHookStorage)
	r := httptest.NewRequest("POST", "/", bytes.NewBuffer([]byte("")))
	r = mux.SetURLVars(r, map[string]string{"identifier": encodedInvalidWebhookIdentifier})
	w := httptest.NewRecorder()
	mw(func(w http.ResponseWriter, r *http.Request) {})(w, r)
	if http.StatusNotFound != w.Code {
		t.Errorf("wrong status code set when webhook was not found, got %d but wanted %d", w.Code, http.StatusNotFound)
	}
}

func TestWebhookExists(t *testing.T) {
	t.Log("test so that the middleware check the storage for the requested webhook and let request continue if found")
	mw := middleware.WithCheckIfWebhookExists(&mockHookStorage)
	r := httptest.NewRequest("POST", "/", bytes.NewBuffer([]byte("")))
	r = mux.SetURLVars(r, map[string]string{"identifier": encodedValidWebhookIdentifier})
	w := httptest.NewRecorder()
	mw(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusTeapot)
	})(w, r)
	if http.StatusTeapot != w.Code {
		t.Errorf("wrong status code set when webhook was found, got %d but wanted %d", w.Code, http.StatusTeapot)
	}
}
