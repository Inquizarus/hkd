package middleware

import (
	"fmt"
	"gitlab.com/inquizarus/rest"
	"net"
	"net/http"
)

// WithLocalOriginOnlyAllowed will intercept requests from a non local origin and mark them as unauthorized
func WithLocalOriginOnlyAllowed(lf func(string)) rest.Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			ip, _, _ := net.SplitHostPort(r.RemoteAddr)
			lf(fmt.Sprintf("request coming from IP %s\n", ip))
			if "127.0.0.1" != ip {
				lf(fmt.Sprintf("non localhost IP tried using \"%s\" endpoint, \"%s\"\n", r.URL.Path, ip))
				w.WriteHeader(http.StatusUnauthorized)
				w.Write([]byte("{\"message\":\"action not allowed\"}"))
				return
			}
			f(w, r)
		}
	}
}
