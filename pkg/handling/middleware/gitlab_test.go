package middleware_test

import (
	"bytes"
	"encoding/base64"
	"gitlab.com/inquizarus/hkd/pkg/handling/middleware"
	"gitlab.com/inquizarus/hkd/pkg/logging"
	"gitlab.com/inquizarus/hkd/pkg/mocking"
	"net/http"
	"net/http/httptest"
	"testing"
)

var validGitlabSecret = base64.StdEncoding.EncodeToString([]byte(mocking.ValidTokenIdentifier))
var invalidGitlabSecret = "aW52YWxpZC10b2tlbg=="

var logger = logging.NullLogger{}

func TestThatGitlabRequestIsDeniedIfAuthHeaderIsMissing(t *testing.T) {
	mw := middleware.WithGitlabAuthHeaderValidation(mocking.Storage.TokenReader(), &logger)
	r := httptest.NewRequest("POST", "/", bytes.NewReader([]byte("{}")))
	w := httptest.NewRecorder()
	mwf := mw(func(w http.ResponseWriter, r *http.Request) {
		t.Errorf("validation have failed, gitlab request without auth header was passed to handler")
	})
	mwf(w, r)
}

func TestThatGitlabRequestIsUnAuhtorizedIfAuthHeaderIsPresentButInvalid(t *testing.T) {

	mw := middleware.WithGitlabAuthHeaderValidation(mocking.Storage.TokenReader(), &logger)
	r := httptest.NewRequest("POST", "/", bytes.NewReader([]byte("{}")))
	w := httptest.NewRecorder()
	mwf := mw(func(w http.ResponseWriter, r *http.Request) {})
	r.Header.Set("X-Gitlab-Token", invalidGitlabSecret)
	mwf(w, r)
	if http.StatusUnauthorized != w.Code {
		t.Errorf("validation have failed, gitlab request with invalid auth header was passed to handler, got status code %d but wanted %d", w.Code, http.StatusUnauthorized)
	}
}

func TestThatGitlabRequestIsBadIfAuthHeaderIsPresentButCantBeDecoded(t *testing.T) {

	mw := middleware.WithGitlabAuthHeaderValidation(mocking.Storage.TokenReader(), &logger)
	r := httptest.NewRequest("POST", "/", bytes.NewReader([]byte("{}")))
	w := httptest.NewRecorder()
	mwf := mw(func(w http.ResponseWriter, r *http.Request) {})
	r.Header.Set("X-Gitlab-Token", "abc")
	mwf(w, r)
	if http.StatusBadRequest != w.Code {
		t.Errorf("validation have failed, gitlab request with invalid auth header was passed to handler, got status code %d but wanted %d", w.Code, http.StatusBadRequest)
	}
}

func TestThatGitlabRequestIsUnAuhtorizedIfAuthHeaderIsPresentButEmpty(t *testing.T) {

	mw := middleware.WithGitlabAuthHeaderValidation(mocking.Storage.TokenReader(), &logger)
	r := httptest.NewRequest("POST", "/", bytes.NewReader([]byte("{}")))
	w := httptest.NewRecorder()
	mwf := mw(func(w http.ResponseWriter, r *http.Request) {})
	r.Header.Set("X-Gitlab-Token", "")
	mwf(w, r)
	if http.StatusUnauthorized != w.Code {
		t.Errorf("validation have failed, gitlab request with invalid auth header was passed to handler, got status code %d but wanted %d", w.Code, http.StatusUnauthorized)
	}
}

func TestThatGitlabRequestIsOkIfAuthHeaderIsPresent(t *testing.T) {
	mw := middleware.WithGitlabAuthHeaderValidation(mocking.Storage.TokenReader(), &logger)
	r := httptest.NewRequest("POST", "/", bytes.NewReader([]byte("{}")))
	w := httptest.NewRecorder()
	mwf := mw(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusTeapot)
	})
	r.Header.Set("X-Gitlab-Token", validGitlabSecret)
	mwf(w, r)
	if http.StatusTeapot != w.Code {
		t.Errorf("validation have failed, gitlab request with auth header did not pass to handler, got status code %d but wanted %d", w.Code, http.StatusTeapot)
	}
}
