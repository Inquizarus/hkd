package middleware

import (
	"encoding/base64"
	"github.com/gorilla/mux"
	"gitlab.com/inquizarus/hkd/pkg/storage/hook"
	"gitlab.com/inquizarus/rest"
	"net/http"
)

// WithCheckIfWebhookExists validates if a webhook refered to in
// the incoming request exists and if not returns a 404 response
func WithCheckIfWebhookExists(whr hook.Reader) rest.Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			vars := mux.Vars(r)
			identifier := vars["identifier"]
			if "" == identifier {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			identifierBytes, _ := base64.StdEncoding.DecodeString(identifier)
			_, err := whr.Fetch(string(identifierBytes))
			if nil != err {
				w.WriteHeader(http.StatusNotFound)
				return
			}
			f(w, r)
		}
	}
}
