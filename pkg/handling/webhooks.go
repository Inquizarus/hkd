package handling

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitlab.com/inquizarus/hkd/internal/executing"
	"gitlab.com/inquizarus/hkd/pkg/config"
	"gitlab.com/inquizarus/hkd/pkg/handling/middleware"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"gitlab.com/inquizarus/hkd/pkg/validating"
	"gitlab.com/inquizarus/hkd/pkg/vcs"
	"gitlab.com/inquizarus/rest"
	"io/ioutil"
	"net/http"
)

// MakeTriggerWebhookHandler ...
func MakeTriggerWebhookHandler(cfg config.HkdConfig) *rest.BaseHandler {
	return &rest.BaseHandler{
		Path: "/webhook/{identifier}",
		Name: "webhook-handler",
		Middlewares: []rest.Middleware{
			middleware.WithCheckIfWebhookExists(cfg.Storage().WebhookReadWriter()),
			rest.WithJSONContent(),
		},
		Post: func(w http.ResponseWriter, r *http.Request, vars map[string]string) {
			authorized := false
			response := model.HTTPResponse{
				Status:  http.StatusOK,
				Message: "successfully executed webhook",
			}
			identifier := vars["identifier"]
			decodedIdentifier, _ := base64.StdEncoding.DecodeString(identifier)
			webhook, _ := cfg.Storage().WebhookReader().Fetch(string(decodedIdentifier))
			requestBody, _ := ioutil.ReadAll(r.Body)

			if r.Header.Get(vcs.GitlabDeliveryHeader) != "" {
				authorized = validating.GitlabRequestAllowedToExecuteWebhook(r, cfg.Storage())
			}
			if r.Header.Get(vcs.GithubDeliveryHeader) != "" {
				response.Status = http.StatusNotImplemented
				response.Message = "github support not implemented"
			}
			if r.Header.Get(vcs.GiteaDeliveryHeader) != "" {
				authorized = validating.GiteaRequestAllowedToExecuteWebhook(requestBody, cfg.Storage())
			}

			payload, err := model.MakeDefaultPayloadFromBody(bytes.NewReader(requestBody))

			if nil != err {
				response.Status = http.StatusBadRequest
				response.Message = "malformed payload in request"
			}

			if false == authorized {
				authorized = validating.RequestAllowedToExecuteWebhook(r, cfg.Storage())
			}

			if response.Status == http.StatusOK && false == authorized {
				response.Status = http.StatusUnauthorized
				response.Message = "invalid authorization token provided"
			}

			// If nothing bad have happened we can try to execute the webhook
			if http.StatusOK == response.Status {
				output, err := executing.Webhook(webhook, payload)
				if nil != err {
					response.Status = http.StatusInternalServerError
					response.Message = "an internal error occured"
				}
				response.Output = output
			}
			bbytes, _ := response.JSON()
			w.WriteHeader(response.Status)
			w.Write(bbytes)
		},
	}
}

// MakeCreateWebhookHandler ...
func MakeCreateWebhookHandler(cfg config.HkdConfig) *rest.BaseHandler {
	return &rest.BaseHandler{
		Path: "/webhook",
		Name: "create-webhook-handler",
		Middlewares: []rest.Middleware{
			rest.WithJSONContent(),
			middleware.WithTokenValidation(cfg.Storage().TokenReader(), cfg.Logger()),
			middleware.WithLocalOriginOnlyAllowed(func(msg string) {
				cfg.Logger().Debug(msg)
			}),
		},
		Post: func(w http.ResponseWriter, r *http.Request, vars map[string]string) {
			defer r.Body.Close()
			payload, err := ioutil.ReadAll(r.Body)
			if nil != err {
				cfg.Logger().Error(err.Error())
				cfg.Printer().PrintText("could not create webhook")
				setInternalServerError(w, "could not create webhook")
				return
			}

			cfg.Logger().Info(fmt.Sprintf("webhook payload \"%s\"", payload))

			wh := model.Webhook{}
			b := []byte(payload)
			err = json.Unmarshal(b, &wh)
			if nil != err {
				cfg.Logger().Error(err.Error())
				cfg.Printer().PrintText("could not create webhook")
				setInternalServerError(w, "could not create webhook")
				return
			}

			err = cfg.Storage().WebhookWriter().Create(wh.Identifier, wh)

			if nil != err {
				cfg.Logger().Error(err.Error())
				cfg.Printer().PrintText("could not create webhook")
				setInternalServerError(w, "could not create webhook")
				return
			}
			setOk(w, "webhook created")
		},
	}
}

// MakeListHooksHandler allows listing information about one or all hooks registered currently
func MakeListHooksHandler(cfg config.HkdConfig) *rest.BaseHandler {
	return &rest.BaseHandler{
		Path: "/webhooks",
		Name: "list-hooks-handler",
		Middlewares: []rest.Middleware{
			rest.WithJSONContent(),
			middleware.WithTokenValidation(cfg.Storage().TokenReader(), cfg.Logger()),
			middleware.WithLocalOriginOnlyAllowed(func(msg string) {
				cfg.Logger().Debug(msg)
			}),
		},
		Get: func(w http.ResponseWriter, r *http.Request, vars map[string]string) {
			var err error
			var webhooks []model.Webhook
			var body struct {
				Message string          `json:"message"`
				Hooks   []model.Webhook `json:"webhooks"`
			}
			body.Message = "ok"
			identifier := vars["webhook"]
			webhooks, err = cfg.Storage().WebhookReader().FetchAll()

			if 1 > len(webhooks) {
				body.Message = "no webhooks registered"
			}

			body.Hooks = append(body.Hooks, webhooks...)

			bbytes, err := json.Marshal(body)

			if nil == err {
				w.WriteHeader(http.StatusOK)
				w.Write(bbytes)
				return
			}

			cfg.Logger().Error(fmt.Sprintf("could not resolve info for %s, \"%s\"", identifier, err.Error()))
			setNotFound(w, "could not find info for webhook")
		},
	}
}
