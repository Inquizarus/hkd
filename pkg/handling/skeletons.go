package handling

import (
	//	"encoding/json"
	"encoding/json"
	"gitlab.com/inquizarus/hkd/pkg/config"
	"gitlab.com/inquizarus/hkd/pkg/generating"
	"gitlab.com/inquizarus/rest"
	"net/http"
)

// MakeSkeletonHandler ...
func MakeSkeletonHandler(cfg config.HkdConfig) *rest.BaseHandler {
	return &rest.BaseHandler{
		Path: "/skeletons/{type}",
		Name: "create-token-skeleton-handler",
		Middlewares: []rest.Middleware{
			rest.WithJSONContent(),
		},
		Get: func(w http.ResponseWriter, r *http.Request, vars map[string]string) {
			defer r.Body.Close()

			t := vars["type"]

			w.WriteHeader(http.StatusOK)

			if "token" == t {
				c := generating.TokenSkeleton()
				b, _ := json.Marshal(c)
				w.Write(b)
				return
			}

			if "webhook" == t {
				c := generating.WebhookSkeleton()
				b, _ := json.Marshal(c)
				w.Write(b)
				return
			}

			w.WriteHeader(http.StatusNotFound)
		},
	}
}
