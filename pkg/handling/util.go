package handling

import (
	"fmt"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"net"
	"net/http"
)

func setBadRequest(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(buildBodyString(msg)))
}

func setUnauthorized(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusUnauthorized)
	w.Write([]byte(buildBodyString(msg)))
}

func setInternalServerError(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(buildBodyString(msg)))
}

func setNotFound(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte(buildBodyString(msg)))
}

func setOk(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(buildBodyString(msg)))
}

func buildBodyString(msg string) string {
	return fmt.Sprintf("{\"message\":\"%s\"}", msg)
}

func originIsAllowedForHook(wh model.Webhook, origin string) bool {
	for _, o := range wh.AllowedOrigins {
		if "*" == o || origin == o {
			return true
		}
	}
	return false
}

func getIPFromAddress(h string) string {
	ip, _, _ := net.SplitHostPort(h)
	return ip
}

func isLocalIP(ip string) bool {
	if "127.0.0.1" == ip {
		return true
	}
	return false
}

func isNotLocalIP(ip string) bool {
	return !isLocalIP(ip)
}
