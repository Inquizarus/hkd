package handling_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/inquizarus/hkd/pkg/config"
	"gitlab.com/inquizarus/hkd/pkg/handling"
	"gitlab.com/inquizarus/hkd/pkg/mocking"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"net/http"
	"net/http/httptest"
	"testing"
)

func s() string { return "" }

func TestThatTokensHandlerHaveCorrectPath(t *testing.T) {
	h := handling.MakeTokenHandler(&config.NullHkdConfig{}, s, s)
	expected := "/tokens"
	if expected != h.GetPath() {
		t.Errorf("incorrect path for tokens handler, got \"%s\" but expected \"%s\"", h.GetPath(), expected)
	}
}

func TestThatTokensHandlerHaveCorrectName(t *testing.T) {
	h := handling.MakeTokenHandler(&config.NullHkdConfig{}, s, s)
	expected := "token-handler"
	if expected != h.GetName() {
		t.Errorf("incorrect name for tokens handler, got \"%s\" but expected \"%s\"", h.GetName(), expected)
	}
}

func TestThatTokensHandlerSetJsonContent(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	cfg := b.Build()
	t.Log("ensure that application/json is set as content type for response")
	h := handling.MakeTokenHandler(cfg, s, s)
	r := httptest.NewRequest("POST", h.GetPath(), bytes.NewReader([]byte("")))
	r.RemoteAddr = "127.0.0.1:1234"
	w := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := "application/json"
	actual := w.Header().Get("content-type")
	if expected != actual {
		t.Errorf("incorrect content-type, got \"%s\" but expected \"%s\"", actual, expected)
	}
}

func TestThatTokensHandlerReturnBadRequestForMalformedPayload(t *testing.T) {
	b := config.StandardHkdConfigBuilder{}
	cfg := b.Build()
	h := handling.MakeTokenHandler(cfg, s, s)
	r := httptest.NewRequest("POST", h.GetPath(), bytes.NewReader([]byte(`{"asd":24#¤#&&&`)))
	r.RemoteAddr = "127.0.0.1:1234"
	w := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusBadRequest
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect HTTP Status Code when malformed payload was sent, got \"%d\" but expected \"%d\"", actual, expected)
	}
}

func TestThatTokensHandlerReturnInternalServerError(t *testing.T) {
	t.Log("whenever insertion into storage fails an internal server error should be returned to client")
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	b.WithHasher(func(b []byte) []byte {
		return b
	})
	cfg := b.Build()
	h := handling.MakeTokenHandler(cfg, s, s)
	r := httptest.NewRequest("POST", h.GetPath(), bytes.NewReader(
		[]byte(
			fmt.Sprintf(`{"identifier":"%s"}`, mocking.InsertErrorTokenIdentifier),
		),
	))
	r.RemoteAddr = "127.0.0.1:1234"
	w := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusInternalServerError
	actual := w.Code
	if expected != actual {
		t.Errorf("incorrect HTTP Status Code when trying to create token that should result in error, got \"%d\" but expected \"%d\"", actual, expected)
	}
}

func TestThatTokensHandlerReturnOkWhenTokenIsCreated(t *testing.T) {
	t.Log("whenever insertion into storage succeeds a 200 OK status code should be returned to client")
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	b.WithHasher(mocking.MockHasher)
	cfg := b.Build()
	h := handling.MakeTokenHandler(
		cfg,
		func() string { return mocking.TokenIdentifierPrefix },
		func() string { return mocking.InsertSuccessTokenIdentifierKey },
	)
	r := httptest.NewRequest("POST", h.GetPath(), bytes.NewReader(
		[]byte(
			fmt.Sprintf(`{"identifier":"%s"}`, "new-token"),
		),
	))
	r.RemoteAddr = "127.0.0.1:1234"
	w := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	expected := http.StatusOK
	actual := w.Code
	if expected != actual {
		t.Errorf(
			"incorrect HTTP Status Code when trying to create token, got \"%d\" but expected \"%d\"",
			actual,
			expected,
		)
	}
}

func TestThatTokenIdentifierIsGenerated(t *testing.T) {
	t.Log("whenever a token is created a new unique identifier should be generated for it")
	expectedPrefix := mocking.TokenIdentifierPrefix
	expectedKey := mocking.InsertSuccessTokenIdentifierKey
	expected := fmt.Sprintf("%s.%s", expectedPrefix, expectedKey)
	b := config.StandardHkdConfigBuilder{}
	b.WithStorage(mocking.Storage)
	b.WithHasher(mocking.MockHasher)
	cfg := b.Build()
	h := handling.MakeTokenHandler(
		cfg,
		func() string { return expectedPrefix },
		func() string { return expectedKey },
	)
	r := httptest.NewRequest("POST", h.GetPath(), bytes.NewReader(
		[]byte(
			fmt.Sprintf(`{"description":"abc", "allowed-origins":["192.168.10.123"]}`),
		),
	))
	r.RemoteAddr = "127.0.0.1:1234"
	w := httptest.NewRecorder()
	router := mux.NewRouter()
	router.Handle(h.GetPath(), h)
	router.ServeHTTP(w, r)
	createTokenResponse := model.HTTPResponse{}
	json.Unmarshal(w.Body.Bytes(), &createTokenResponse)
	actual := createTokenResponse.Message
	if expected != actual {
		t.Errorf(
			"incorrect token identifier generated and returned, got \"%s\" but expected \"%s\"",
			actual,
			expected,
		)
	}
}
