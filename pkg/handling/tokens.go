package handling

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/inquizarus/hkd/pkg/config"
	"gitlab.com/inquizarus/hkd/pkg/handling/middleware"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"gitlab.com/inquizarus/rest"
)

// MakeTokenHandler returns handler for creating and updating a token
func MakeTokenHandler(cfg config.HkdConfig, rs func() string, us func() string) *rest.BaseHandler {
	lf := func(msg string) {
		cfg.Logger().Info(msg)
	}
	return &rest.BaseHandler{
		Path: "/tokens",
		Name: "token-handler",
		Middlewares: []rest.Middleware{
			rest.WithJSONContent(),
			middleware.WithLocalOriginOnlyAllowed(lf),
		},
		Get: func(w http.ResponseWriter, r *http.Request, vars map[string]string) {
			tokens, err := cfg.Storage().TokenReader().FetchAll()
			if nil != err {
				setInternalServerError(w, "could not load token data")
				return
			}
			if 1 > len(tokens) {
				setInternalServerError(w, "no tokens in storage")
				return
			}

			bbytes, err := json.Marshal(tokens)

			if nil == err {
				w.WriteHeader(http.StatusOK)
				w.Write(bbytes)
				return
			}
			setInternalServerError(w, "could not handle the request")
		},
		Post: func(w http.ResponseWriter, r *http.Request, _ map[string]string) {

			payload := model.CreateTokenPayload{}
			bb, err := ioutil.ReadAll(r.Body)

			err = json.Unmarshal(bb, &payload)

			if nil != err {
				setBadRequest(w, "bad payload sent")
				return
			}

			prefix := rs()
			key := us()
			full := fmt.Sprintf("%s.%s", prefix, key)
			token := model.Token{
				AllowedOrigins: payload.AllowedOrigins,
				Name:           payload.Name,
				Prefix:         prefix,
				Hash:           cfg.Hash(full),
			}

			err = cfg.Storage().TokenWriter().Create(token.String(), token)
			if nil != err {
				setInternalServerError(w, err.Error())
				return
			}
			setOk(w, full)
		},
	}
}
