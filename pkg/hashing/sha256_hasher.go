package hashing

import "crypto/sha256"

// Sha256 implements Hasher for sha256 method
func Sha256(input []byte) []byte {
	sum := sha256.Sum256(input)
	var hash []byte
	for index := 0; index < len(sum); index++ {
		hash = append(hash, sum[index])
	}
	return hash
}
