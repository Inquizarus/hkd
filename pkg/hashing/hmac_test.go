package hashing_test

import (
	"gitlab.com/inquizarus/hkd/pkg/hashing"
	"gitlab.com/inquizarus/hkd/pkg/util"
	"testing"
)

func TestThatHmacReturnsCorrectValue(t *testing.T) {
	t.Log("tests that hmac Hasher have been correctly implemented")
	input := []byte("testing hmac hashing")
	secret := []byte("secret_key")
	expected := "28b4da98b92f6f2792fc4856ad0c4136ef567187"
	hmac := hashing.MakeHmac(secret)
	actual := hmac(input)
	if true != util.EqualByteSlices([]byte(expected), actual) {
		t.Errorf("Hmac Hasher resulted in wrong value, expected %x but got %x", expected, actual)
	}
}
