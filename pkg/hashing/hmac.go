package hashing

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
)

// MakeHmac returns a Hasher func with the secret curried unto hmac hashing function
func MakeHmac(secret []byte) Hasher {
	return func(input []byte) []byte {
		h := hmac.New(sha1.New, secret)
		h.Write(input)
		return []byte(hex.EncodeToString(h.Sum(nil)))
	}
}
