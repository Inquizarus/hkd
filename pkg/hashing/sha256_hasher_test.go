package hashing_test

import (
	"encoding/hex"
	"gitlab.com/inquizarus/hkd/pkg/hashing"
	"gitlab.com/inquizarus/hkd/pkg/util"
	"testing"
)

func TestThatSha256ReturnsCorrectValue(t *testing.T) {
	t.Log("tests that sha256 Hasher have been correctly implemented")
	input := []byte("testing sha256 hashing")
	expected, _ := hex.DecodeString("80065a511092b4d511bcc1e07922a74350fa8e87b9bb1756e54dccb8cfa636c8")
	actual := hashing.Sha256(input)
	if true != util.EqualByteSlices(expected, actual) {
		t.Errorf("Sha256 Hasher resulted in wrong value, expected %x but got %x", expected, actual)
	}
}
