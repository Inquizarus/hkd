package hashing

// Hasher function interface
type Hasher func([]byte) []byte
