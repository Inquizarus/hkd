package logging

import (
	"github.com/sirupsen/logrus"
	lSyslog "github.com/sirupsen/logrus/hooks/syslog"
	"io/ioutil"
	"log/syslog"
)

// DefaultLogger for hkd
type DefaultLogger struct {
	log *logrus.Logger
}

// Info implementation for DefaultLogger
func (dl *DefaultLogger) Info(msg string) {
	dl.log.Info(msg)
}

// Error implementation for DefaultLogger
func (dl *DefaultLogger) Error(msg string) {
	dl.log.Error(msg)
}

// Debug implementation for DefaultLogger
func (dl *DefaultLogger) Debug(msg string) {
	dl.log.Debug(msg)
}

// MakeDefaultLogger instantiates all dependencies in the logger and returns it
func MakeDefaultLogger() Logger {
	log := logrus.New()
	log.Out = ioutil.Discard
	info, _ := lSyslog.NewSyslogHook("", "", syslog.LOG_INFO, "")
	debug, _ := lSyslog.NewSyslogHook("", "", syslog.LOG_DEBUG, "")
	errorl, _ := lSyslog.NewSyslogHook("", "", syslog.LOG_ERR, "")
	hooks := logrus.LevelHooks{
		logrus.InfoLevel:  []logrus.Hook{info},
		logrus.DebugLevel: []logrus.Hook{debug},
		logrus.ErrorLevel: []logrus.Hook{errorl},
	}
	log.Hooks = hooks
	log.Level = logrus.DebugLevel
	log.SetFormatter(&logrus.JSONFormatter{})
	return &DefaultLogger{
		log: log,
	}
}
