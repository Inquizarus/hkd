package logging

// NullLogger implementation for Logger interface
type NullLogger struct{}

// Info implementation for NullLogger
func (dl *NullLogger) Info(msg string) {}

// Error implementation for NullLogger
func (dl *NullLogger) Error(msg string) {}

// Debug implementation for NullLogger
func (dl *NullLogger) Debug(msg string) {}
