package logging

// Logger interface
type Logger interface {
	Info(string)
	Error(string)
	Debug(string)
}
