package hook_test

import (
	"bytes"
	"testing"

	"gitlab.com/inquizarus/hkd/pkg/storage/hook"
)

func TestThatFileHookReaderCanFetchAllHooks(t *testing.T) {
	c := bytes.NewBuffer([]byte(`[{"identifier":"abc"}, {"identifier":"123"}]`))
	r := hook.FileReader{Content: c}
	result, err := r.FetchAll()
	if nil != err {
		t.Errorf("got error when fetching all hooks when not expected, \"%s\"", err)
	}
	if 2 > len(result) {
		t.Errorf("wrong number of hooks fetched, expected %d but got %d", 2, len(result))
	}
}

func TestThatFileHookReaderCanFetchAllHooksMultipleTimes(t *testing.T) {
	c := bytes.NewBuffer([]byte(`[{"identifier":"abc"}, {"identifier":"123"}]`))
	r := hook.FileReader{Content: c}
	_, err := r.FetchAll()
	if nil != err {
		t.Errorf("got error on first call when fetching all hooks when not expected, \"%s\"", err)
	}
	_, err = r.FetchAll()
	if nil != err {
		t.Errorf("got error on second call when fetching all hooks when not expected, \"%s\"", err)
	}
}

func TestThatFileHookReaderCanFetchSpecificHook(t *testing.T) {
	i := "123"
	c := bytes.NewBuffer([]byte(`[{"identifier":"abc"}, {"identifier":"123"}]`))
	r := hook.FileReader{Content: c}
	hook, err := r.Fetch(i)
	if nil != err {
		t.Errorf("got error when fetching specific hook when not expected, \"%s\"", err)
	}
	if i != hook.Identifier {
		t.Errorf("wrong hook returned when fetching specific one, got hook with identifier %s but wanted with %s", hook.Identifier, i)
	}
}

func TestThatFileHookReaderReturnErrorWhenHookCantBeFound(t *testing.T) {
	i := "123"
	c := bytes.NewBuffer([]byte(`[{"identifier":"abc"}]`))
	r := hook.FileReader{Content: c}
	_, err := r.Fetch(i)
	if nil == err {
		t.Error("did not get error when fetching specific hook when expected")
	}
}
