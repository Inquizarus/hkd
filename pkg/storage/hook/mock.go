package hook

import (
	"errors"
	"gitlab.com/inquizarus/hkd/pkg/model"
)

// MockReadWriter implementation of ReadWriter interface and always return empty results and nil errors
type MockReadWriter struct {
	FetchResults map[string]model.Webhook
}

// Fetch implementation for MockReadWriter result based on FetchResults map
func (rw *MockReadWriter) Fetch(i string) (model.Webhook, error) {
	if t, ok := rw.FetchResults[i]; ok {
		return t, nil
	}
	return model.Webhook{}, errors.New("Hook does not exist")
}

// FetchAll implementation for MockReadWriter returns all Hooks in FetchResults maps
func (rw *MockReadWriter) FetchAll() (Hooks []model.Webhook, err error) {
	for _, t := range rw.FetchResults {
		Hooks = append(Hooks, t)
	}
	return Hooks, err
}

// Create implementation for MockReadWriter returns nil error
func (rw *MockReadWriter) Create(i string, t model.Webhook) (err error) {
	return err
}

// Update implementation for MockReadWriter returns nil error
func (rw *MockReadWriter) Update(i string, t model.Webhook) (err error) {
	return err
}

// Remove implementation for MockReadWriter returns nil error
func (rw *MockReadWriter) Remove(i string) (err error) {
	return err
}
