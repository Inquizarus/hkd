package hook

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/boltdb/bolt"
	"os"
	"time"

	"gitlab.com/inquizarus/hkd/pkg/model"
)

// Reader interface for reading webhooks from a storage
type Reader interface {
	Fetch(string) (model.Webhook, error)
	FetchAll() ([]model.Webhook, error)
}

// Writer interface for writing webhooks to storage
type Writer interface {
	Create(string, model.Webhook) error
	Remove(string) error
	Update(string, model.Webhook) error
}

// ReadWriter combines interfaces of hook Reader and Writer
type ReadWriter interface {
	Reader
	Writer
}

// BoltReadWriter uses BoltDB for handling webhooks
type BoltReadWriter struct {
	Path          string
	WebhookBucket string
}

// Fetch implementation for BoltReadWriter
func (rw *BoltReadWriter) Fetch(i string) (model.Webhook, error) {
	var err error
	var webhook model.Webhook
	db, err := bolt.Open(rw.Path, 0600, nil)
	if nil == err {
		defer db.Close()
		webhook, err = rw.fetch(i, db)
	}
	return webhook, err
}

// FetchAll implementation for BoltReadWriter
func (rw *BoltReadWriter) FetchAll() ([]model.Webhook, error) {
	var err error
	var webhooks []model.Webhook
	db, err := bolt.Open(rw.Path, 0600, nil)
	if nil == err {
		defer db.Close()
		webhooks, err = rw.fetchAll(db)
	}
	return webhooks, err
}

// Create implementation for BoltReadWriter
func (rw *BoltReadWriter) Create(i string, webhook model.Webhook) error {
	var err error

	boltOptions := bolt.Options{
		Timeout: time.Second * 5,
	}

	if db, err := bolt.Open(rw.Path, 0600, &boltOptions); nil == err {
		defer db.Close()
		err = rw.create(i, webhook, db)
	}

	return err
}

// Remove implementation for BoltReadWriter
func (rw *BoltReadWriter) Remove(i string) error {
	var err error
	db, err := bolt.Open(rw.Path, os.ModePerm, nil)
	if nil == err {
		defer db.Close()
		_, err = rw.fetch(i, db)
		if nil == err {
			err = rw.remove(i, db)
		}
	}
	return err
}

// Update implementation for BoltReadWriter
func (rw *BoltReadWriter) Update(i string, webhook model.Webhook) error {
	var err error
	db, err := bolt.Open(rw.Path, 0600, &bolt.Options{
		Timeout: time.Second * 5,
	})
	if nil == err {
		defer db.Close()
		if _, e := rw.fetch(i, db); e == nil {
			err = rw.create(i, webhook, db)
		}

	}
	return err
}

func (rw *BoltReadWriter) fetch(i string, db *bolt.DB) (model.Webhook, error) {
	var webhook model.Webhook
	var err error
	err = db.Update(func(tx *bolt.Tx) error {
		b := rw.getWebhookBucket(tx)
		if nil != b {
			c := b.Cursor()
			k, v := c.Seek([]byte(i))
			if len(k) > 0 {
				return json.Unmarshal(v, &webhook)
			}
			return fmt.Errorf("webhook %s was not found", i)
		}
		return errors.New("bucket was not found for webhooks")
	})
	return webhook, err
}

func (rw *BoltReadWriter) fetchAll(db *bolt.DB) ([]model.Webhook, error) {
	var err error
	var webhooks []model.Webhook
	err = db.Update(func(tx *bolt.Tx) error {
		b := rw.getWebhookBucket(tx)
		if nil != b {
			c := b.Cursor()
			for k, v := c.First(); k != nil; k, v = c.Next() {
				webhook := model.Webhook{}
				err = json.Unmarshal(v, &webhook)
				if nil != err {
					fmt.Printf("could not unmarshal webhook with key %s", k)
					continue
				}
				webhooks = append(webhooks, webhook)
			}
			return nil
		}
		return errors.New("bucket was not found for webhooks")
	})
	return webhooks, err
}

func (rw *BoltReadWriter) create(i string, webhook model.Webhook, db *bolt.DB) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := rw.getWebhookBucket(tx)
		data, err := json.Marshal(webhook)
		if nil == err {
			err = b.Put([]byte(i), data)
		}
		return err
	})
}

func (rw *BoltReadWriter) remove(i string, db *bolt.DB) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := rw.getWebhookBucket(tx)
		return b.Delete([]byte(i))
	})
}

func (rw *BoltReadWriter) getWebhookBucket(tx *bolt.Tx) *bolt.Bucket {
	var b *bolt.Bucket
	var err error
	b, err = tx.CreateBucketIfNotExists([]byte(rw.WebhookBucket))
	if nil != err {
		fmt.Printf("bucket %s did not exist and could not create it \"%s\"\n", rw.WebhookBucket, err)
	}
	return b
}

// FileReader uses a file path to load hooks
type FileReader struct {
	Content *bytes.Buffer
}

// Fetch implementation for FileReader
func (r *FileReader) Fetch(i string) (model.Webhook, error) {
	var err error
	var hook model.Webhook
	var hooks []model.Webhook

	hooks, err = r.FetchAll()

	if nil == err {
		for _, h := range hooks {
			if h.Identifier == i {
				return h, nil
			}
		}
	}
	if nil == err {
		err = fmt.Errorf("could not find hook with identifier %s", i)
	}
	return hook, err
}

// FetchAll implementation for FileReader
func (r *FileReader) FetchAll() ([]model.Webhook, error) {
	var err error
	var hooks []model.Webhook
	if nil == err {
		err = json.Unmarshal(r.Content.Bytes(), &hooks)
	}
	return hooks, err
}

// NullReadWriter implementation of ReadWriter interface and always return empty results and nil errors
type NullReadWriter struct{}

// Fetch implementation for NullReadWriter returns empty webhook and nil error
func (rw *NullReadWriter) Fetch(i string) (webhook model.Webhook, err error) {
	return webhook, err
}

// FetchAll implementation for NullReadWriter returns empty webhook slice and nil error
func (rw *NullReadWriter) FetchAll() (webhooks []model.Webhook, err error) {
	return webhooks, err
}

// Create implementation for NullReadWriter returns nil error
func (rw *NullReadWriter) Create(i string, t model.Webhook) (err error) {
	return err
}

// Update implementation for NullReadWriter returns nil error
func (rw *NullReadWriter) Update(i string, t model.Webhook) (err error) {
	return err
}

// Remove implementation for NullReadWriter returns nil error
func (rw *NullReadWriter) Remove(i string) (err error) {
	return err
}
