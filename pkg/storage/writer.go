package storage

import (
	"gitlab.com/inquizarus/hkd/pkg/storage/hook"
	"gitlab.com/inquizarus/hkd/pkg/storage/token"
)

// Writer interface for storage
type Writer interface {
	TokenWriter() token.Writer
	WebhookWriter() hook.Writer
}
