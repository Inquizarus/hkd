package storage

import (
	"gitlab.com/inquizarus/hkd/pkg/storage/hook"
	"gitlab.com/inquizarus/hkd/pkg/storage/token"
)

// Reader interface for storage
type Reader interface {
	TokenReader() token.Reader
	WebhookReader() hook.Reader
}
