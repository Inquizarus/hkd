package storage

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"gitlab.com/inquizarus/hkd/pkg/storage/hook"
	"gitlab.com/inquizarus/hkd/pkg/storage/token"
)

// ReadWriter storage interface
type ReadWriter interface {
	Reader
	Writer
	TokenReadWriter() token.ReadWriter
	WebhookReadWriter() hook.ReadWriter
}

// StandardReadWriter implementation of ReadRwiter interface
type StandardReadWriter struct {
	trw token.ReadWriter
	hrw hook.ReadWriter
}

// TokenReader implementation for StandardReadWriter
func (brw *StandardReadWriter) TokenReader() token.Reader {
	return brw.trw
}

// WebhookReader implementation for StandardReadWriter
func (brw *StandardReadWriter) WebhookReader() hook.Reader {
	return brw.hrw
}

// TokenWriter implementation for StandardReadWriter
func (brw *StandardReadWriter) TokenWriter() token.Writer {
	return brw.trw
}

// WebhookWriter implementation for StandardReadWriter
func (brw *StandardReadWriter) WebhookWriter() hook.Writer {
	return brw.hrw
}

// WebhookReadWriter implementation for StandardReadWriter
func (brw *StandardReadWriter) WebhookReadWriter() hook.ReadWriter {
	return brw.hrw
}

// TokenReadWriter implementation for StandardReadWriter
func (brw *StandardReadWriter) TokenReadWriter() token.ReadWriter {
	return brw.trw
}

// MakeStandardReadWriter initiates and returns a StandardReadWriter implementation of ReadWriter
func MakeStandardReadWriter() ReadWriter {
	dbPath := fmt.Sprintf("%s/hkd.db", viper.GetString("data-dir"))
	return &StandardReadWriter{
		trw: &token.BoltReadWriter{
			Path:        dbPath,
			TokenBucket: "tokens",
		},
		hrw: &hook.BoltReadWriter{
			Path:          dbPath,
			WebhookBucket: "webhooks",
		},
	}
}

// NullReadWriter implementation of ReadRwiter interface
type NullReadWriter struct {
	trw token.ReadWriter
	hrw hook.ReadWriter
}

// TokenReader implementation for NullReadWriter
func (nrw *NullReadWriter) TokenReader() token.Reader {
	return nrw.trw
}

// WebhookReader implementation for NullReadWriter
func (nrw *NullReadWriter) WebhookReader() hook.Reader {
	return nrw.hrw
}

// TokenWriter implementation for NullReadWriter
func (nrw *NullReadWriter) TokenWriter() token.Writer {
	return nrw.trw
}

// WebhookWriter implementation for NullReadWriter
func (nrw *NullReadWriter) WebhookWriter() hook.Writer {
	return nrw.hrw
}

// WebhookReadWriter implementation for NullReadWriter
func (nrw *NullReadWriter) WebhookReadWriter() hook.ReadWriter {
	return nrw.hrw
}

// TokenReadWriter implementation for NullReadWriter
func (nrw *NullReadWriter) TokenReadWriter() token.ReadWriter {
	return nrw.trw
}

// MakeMockReadWriter ...
func MakeMockReadWriter(
	tmap map[string]model.Token,
	ctmap map[string]error,
	hmap map[string]model.Webhook,
) ReadWriter {
	return &StandardReadWriter{
		trw: &token.MockReadWriter{
			FetchResults:  tmap,
			InsertResults: ctmap,
		},
		hrw: &hook.MockReadWriter{
			FetchResults: hmap,
		},
	}
}
