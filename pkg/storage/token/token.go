package token

import (
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"github.com/boltdb/bolt"
	"gitlab.com/inquizarus/hkd/pkg/model"
)

// Reader interface for reading tokens from a storage
type Reader interface {
	Fetch(string) (model.Token, error)
	FetchAll() ([]model.Token, error)
}

// Writer interface for writing tokens to storage
type Writer interface {
	Create(string, model.Token) error
	Remove(string) error
	Update(string, model.Token) error
}

// ReadWriter combines the reader and writer interface
type ReadWriter interface {
	Reader
	Writer
}

// BoltReadWriter uses BoltDB for handling tokens
type BoltReadWriter struct {
	Path        string
	TokenBucket string
}

// Fetch implementation for BoltReadWriter
func (rw *BoltReadWriter) Fetch(i string) (model.Token, error) {
	var err error
	var token model.Token
	db, err := rw.getBoltDB()
	defer db.Close()
	if nil == err {
		return rw.fetch(i, db)
	}
	return token, err
}

// FetchAll implementation for BoltReadWriter
func (rw *BoltReadWriter) FetchAll() ([]model.Token, error) {
	var err error
	var tokens []model.Token
	db, err := rw.getBoltDB()
	defer db.Close()
	if nil == err {
		return rw.fetchAll(db)
	}
	return tokens, err
}

// Create implementation for BoltReadWriter
func (rw *BoltReadWriter) Create(i string, token model.Token) error {
	db, err := rw.getBoltDB()
	defer db.Close()
	if nil == err {
		return rw.create(i, token, db)
	}
	return err
}

// Remove implementation for BoltReadWriter
func (rw *BoltReadWriter) Remove(i string) error {
	db, err := rw.getBoltDB()
	defer db.Close()
	if nil == err {
		return rw.remove(i, db)
	}
	return err
}

// Update implementation for BoltReadWriter
func (rw *BoltReadWriter) Update(i string, token model.Token) error {
	var err error
	db, err := rw.getBoltDB()
	defer db.Close()
	if nil == err {
		if _, e := rw.fetch(i, db); e == nil {
			return rw.create(i, token, db)
		}

	}
	return err
}

func (rw *BoltReadWriter) fetch(i string, db *bolt.DB) (model.Token, error) {
	var token model.Token
	var err error
	err = db.Update(func(tx *bolt.Tx) error {
		b := rw.getTokenBucket(tx)
		if nil != b {
			t := b.Get([]byte(i))
			if nil != t {
				return json.Unmarshal(t, &token)
			}
			return fmt.Errorf("token %s was not found", i)
		}
		return errors.New("bucket was not found for tokens")
	})
	return token, err
}

func (rw *BoltReadWriter) fetchAll(db *bolt.DB) ([]model.Token, error) {
	var err error
	var tokens []model.Token
	err = db.Update(func(tx *bolt.Tx) error {
		b := rw.getTokenBucket(tx)
		if nil != b {
			c := b.Cursor()
			for k, v := c.First(); k != nil; k, v = c.Next() {
				token := model.Token{}
				err = json.Unmarshal(v, &token)
				if nil != err {
					fmt.Printf("could not unmarshal token with key %s", k)
					continue
				}
				tokens = append(tokens, token)
			}
			return nil
		}
		return errors.New("bucket was not found for tokens")
	})
	return tokens, err
}

func (rw *BoltReadWriter) create(i string, token model.Token, db *bolt.DB) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := rw.getTokenBucket(tx)
		data, err := json.Marshal(token)
		if nil == err {
			err = b.Put([]byte(i), data)
		}
		return err
	})
}

func (rw *BoltReadWriter) remove(i string, db *bolt.DB) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := rw.getTokenBucket(tx)
		c := b.Cursor()
		if k, _ := c.Seek([]byte(i)); k == nil || string(k) != i {
			return fmt.Errorf("could not find token with identifier %s", i)
		}
		return c.Delete()

	})
}

func (rw *BoltReadWriter) getBoltDB() (*bolt.DB, error) {
	return bolt.Open(rw.Path, 0640, &bolt.Options{
		Timeout: time.Second * 5,
	})
}

func (rw *BoltReadWriter) getTokenBucket(tx *bolt.Tx) *bolt.Bucket {
	var b *bolt.Bucket
	var err error
	b, err = tx.CreateBucketIfNotExists([]byte(rw.TokenBucket))
	if nil != err {
		fmt.Printf("bucket %s did not exist and could not create it \"%s\"\n", rw.TokenBucket, err)
	}
	return b
}

// NullReadWriter implementation of ReadWriter interface and always return empty results and nil errors
type NullReadWriter struct{}

// Fetch implementation for NullReadWriter returns empty token and nil error
func (rw *NullReadWriter) Fetch(i string) (token model.Token, err error) {
	return token, err
}

// FetchAll implementation for NullReadWriter returns empty token slice and nil error
func (rw *NullReadWriter) FetchAll() (tokens []model.Token, err error) {
	return tokens, err
}

// Create implementation for NullReadWriter returns nil error
func (rw *NullReadWriter) Create(i string, t model.Token) (err error) {
	return err
}

// Update implementation for NullReadWriter returns nil error
func (rw *NullReadWriter) Update(i string, t model.Token) (err error) {
	return err
}

// Remove implementation for NullReadWriter returns nil error
func (rw *NullReadWriter) Remove(i string) (err error) {
	return err
}
