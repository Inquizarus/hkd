package token_test

import (
	"os"
	"testing"

	"gitlab.com/inquizarus/hkd/pkg/model"
	m "gitlab.com/inquizarus/hkd/pkg/model"
	"gitlab.com/inquizarus/hkd/pkg/storage/token"
)

func TestThatBoltReadWriterCanCreateToken(t *testing.T) {

	rw := token.BoltReadWriter{
		Path:        "./hkd.db",
		TokenBucket: "tokens",
	}
	tok := m.Token{
		Name: "test token",
	}
	err := rw.Create("new_token_123", tok)

	if nil != err {
		t.Errorf("error returned when not expected during creation of token %s", err)
	}

	_, err = rw.Fetch("new_token_123")

	if nil != err {
		t.Errorf("error returned when not expected during fetching of token %s", err)
	}

	os.Remove("./hkd.db")
}

func TestThatBoltReadWriterCanUpdateToken(t *testing.T) {

	rw := token.BoltReadWriter{
		Path:        "./hkd.db",
		TokenBucket: "tokens",
	}

	tok := m.Token{
		Name: "test token",
	}

	rw.Create("new_token_123", tok)

	tok.Name = "test_token_updated"

	err := rw.Update("new_token_123", tok)

	if nil != err {
		t.Errorf("error returned when not expected during updating of token %s", err)
	}

	updated, err := rw.Fetch("new_token_123")

	if nil != err {
		t.Errorf("error returned when not expected during fetching of updated token %s", err)
	}

	if updated.Name != "test_token_updated" {
		t.Error("token was same after updating")
	}

	os.Remove("./hkd.db")
}

func TestThatBoltReadWriterCanFetchAll(t *testing.T) {
	rw := token.BoltReadWriter{
		Path:        "./hkd.db",
		TokenBucket: "tokens",
	}
	rw.Create("new_token_123", m.Token{})
	rw.Create("new_token_124", m.Token{})
	tokens, err := rw.FetchAll()
	if nil != err {
		t.Errorf("error returned when not expected during fetching of all tokens %s", err)
	}
	if 2 != len(tokens) {
		t.Errorf("wrong count of tokens returned when fetching, expected %d but got %d", 2, len(tokens))
	}
	os.Remove("./hkd.db")
}

func TestThatBoltReadWriterCanRemoveToken(t *testing.T) {
	rw := token.BoltReadWriter{
		Path:        "./hkd.db",
		TokenBucket: "tokens",
	}
	tok := m.Token{}
	rw.Create("token1", tok)
	err := rw.Remove("token1")
	if nil != err {
		t.Errorf("error returned when not expected during removal of a token %s", err)
	}
	tokens, _ := rw.FetchAll()
	if 0 != len(tokens) {
		t.Errorf("wrong count of tokens returned when fetching after deletion, expected %d but got %d", 0, len(tokens))
	}
	os.Remove("./hkd.db")
}

func TestThatNullReadWriterWorksAsIntended(t *testing.T) {
	t.Log(`This test contains all test cases since it's a null implementation.
		Make sure that no real result is returned from any of the methods.
	`)
	rw := token.NullReadWriter{}

	if ft, err := rw.Fetch("token-id"); err != nil {
		t.Errorf("NullReadWriter Fetch(...) returned something when not expected \"%s, %s\"", ft, err)
	}

	if fat, err := rw.FetchAll(); err != nil || 0 != len(fat) {
		t.Errorf("NullReadWriter FetchAll(...) returned something when not expected \"%s, %s\"", fat, err)
	}

	if err := rw.Create("token-id", model.Token{}); err != nil {
		t.Errorf("NullReadWriter Create(...) returned something when not expected \"%s\"", err)
	}

	if err := rw.Update("token-id", model.Token{}); err != nil {
		t.Errorf("NullReadWriter Update(...) returned something when not expected \"%s\"", err)
	}

	if err := rw.Remove("token-id"); err != nil {
		t.Errorf("NullReadWriter Remove(...) returned something when not expected \"%s\"", err)
	}
}
