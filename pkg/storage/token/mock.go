package token

import (
	"errors"
	"gitlab.com/inquizarus/hkd/pkg/model"
)

// MockReadWriter implementation of ReadWriter interface and always return empty results and nil errors
type MockReadWriter struct {
	FetchResults  map[string]model.Token
	InsertResults map[string]error
}

// Fetch implementation for MockReadWriter result based on FetchResults map
func (rw *MockReadWriter) Fetch(i string) (model.Token, error) {
	if t, ok := rw.FetchResults[i]; ok {
		return t, nil
	}
	return model.Token{}, errors.New("token does not exist")
}

// FetchAll implementation for MockReadWriter returns all tokens in FetchResults maps
func (rw *MockReadWriter) FetchAll() (tokens []model.Token, err error) {
	for _, t := range rw.FetchResults {
		tokens = append(tokens, t)
	}
	return tokens, err
}

// Create implementation for MockReadWriter returns nil error
func (rw *MockReadWriter) Create(i string, t model.Token) (err error) {
	if "" == i {
		return errors.New("blank identifier is not allowed")
	}
	if result, ok := rw.InsertResults[i]; true == ok {
		return result
	}
	return errors.New("token was not in insert result list")
}

// Update implementation for MockReadWriter returns nil error
func (rw *MockReadWriter) Update(i string, t model.Token) (err error) {
	return err
}

// Remove implementation for MockReadWriter returns nil error
func (rw *MockReadWriter) Remove(i string) (err error) {
	return err
}
