package vcs

// GitlabEventHeader ...
const GitlabEventHeader = "X-Gitlab-Event"

// GitlabDeliveryHeader ...
const GitlabDeliveryHeader = GitlabEventHeader

// GitlabTokenHeader ...
const GitlabTokenHeader = "X-Gitlab-Token"
