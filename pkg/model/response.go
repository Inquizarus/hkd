package model

import (
	"encoding/json"
	"net/http"
)

// HTTPResponse is a generic HTTP response model that most endpoints can use
type HTTPResponse struct {
	Status  int    `json:"status,omitempty"`
	Message string `json:"message"`
	Output  string `json:"output,omitempty"`
}

// JSON returns the json marshaled bytes in addition to any error
func (r *HTTPResponse) JSON() ([]byte, error) {
	return json.Marshal(r)
}

// JSONString returns the stringified json structure in addition to any error
func (r *HTTPResponse) JSONString() (string, error) {
	b, err := r.JSON()
	return string(b), err
}

// IsOK returns true if current status is equal to http status OK
func (r *HTTPResponse) IsOK() bool {
	return r.Status == http.StatusOK
}
