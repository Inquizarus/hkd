package model_test

import (
	"encoding/json"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"gitlab.com/inquizarus/hkd/pkg/util"
	"net/http"
	"testing"
)

func TestThatHTTPResponseJSONWorks(t *testing.T) {
	errorPattern := "json bytes was incorrect compared to expected, got %v but wanted %v"
	r := model.HTTPResponse{
		Status:  200,
		Message: "success",
	}
	expectedBytes, _ := json.Marshal(r)
	actualBytes, _ := r.JSON()

	if true != util.EqualByteSlices(expectedBytes, actualBytes) {
		t.Errorf(errorPattern, actualBytes, expectedBytes)
	}
}

func TestThatHTTPResponseJSONStringWorks(t *testing.T) {
	errorPattern := "json strings was incorrect compared to expected, got %s but wanted %s"
	r := model.HTTPResponse{
		Status:  200,
		Message: "success",
	}
	expectedBytes, _ := json.Marshal(r)
	expectedString := string(expectedBytes)
	actualString, _ := r.JSONString()

	if expectedString != actualString {
		t.Errorf(errorPattern, actualString, expectedBytes)
	}
}

func TestThatHTTPResponseIsOKReturnsTrueWhenOK(t *testing.T) {
	response := model.HTTPResponse{
		Status: http.StatusOK,
	}
	if true != response.IsOK() {
		t.Error("IsOK returned false when status was 200")
	}
}

func TestThatHTTPResponseIsOKReturnsFalseWhenNotOK(t *testing.T) {
	response := model.HTTPResponse{
		Status: http.StatusTeapot,
	}
	if false != response.IsOK() {
		t.Error("IsOK returned true when status was 418")
	}
}
