package model

import (
	"fmt"
)

// Token struct for representing a token
type Token struct {
	// Name for this token and it's purpose here on earth
	Name string `json:"name"`
	// AllowedOrigins defines from which sources it can be used, such as gitlab, github or localhost
	AllowedOrigins []string `json:"allowed-origins"`
	// Prefix is prepended to the token hash
	Prefix string `json:"prefix,omitempty"`
	// Hash is the hashed token
	Hash string `json:"hash,omitempty"`
}

// String returns full token with prefix and hash separated by a dot
func (t *Token) String() string {
	if "" == t.Prefix || "" == t.Hash {
		return ""
	}
	return fmt.Sprintf("%s.%s", t.Prefix, t.Hash)
}
