package model_test

import (
	"gitlab.com/inquizarus/hkd/pkg/mocking"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"testing"
)

func TestThatTokenReturnsCorrectString(t *testing.T) {
	cases := []struct {
		T model.Token
		E string
	}{
		{
			T: model.Token{},
			E: "",
		},
		{
			T: model.Token{
				Prefix: mocking.TokenIdentifierPrefix,
				Hash:   string(mocking.ValidTokenHash),
			},
			E: mocking.ValidTokenIdentifier,
		},
	}

	for k, c := range cases {
		actual := c.T.String()
		if c.E != actual {
			t.Errorf("[case %d] Token String() did not return correct value, expected \"%s\" but got \"%s\"", k, c.E, actual)
		}
	}
}
