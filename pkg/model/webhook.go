package model

// Webhook ...
type Webhook struct {
	Identifier       string   `json:"identifier"`
	Description      string   `json:"description"`
	WorkingDirectory string   `json:"command-workdir"`
	ExecuteCommand   string   `json:"execute-command"`
	CommandArgs      []string `json:"command-arguments"`
	AllowedOrigins   []string `json:"allowed-origins"`
	AllowedArguments []string `json:"allowed-arguments"`
	Secret           string   `json:"secret"`
}

// FilterOnAllowedArguments takes a map of string:string and return only the ones
// which is accepted by the webhook definition
func (h *Webhook) FilterOnAllowedArguments(args map[string]string) map[string]string {
	l := map[string]string{}
	for index := 0; index < len(h.AllowedArguments); index++ {
		if v, ok := args[h.AllowedArguments[index]]; ok {
			l[h.AllowedArguments[index]] = v
		}
	}
	return l
}
