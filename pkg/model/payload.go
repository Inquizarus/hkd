package model

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

// Payload interface
type Payload interface {
	GetSecret() string
	ShouldWaitForExecution() bool
	GetArguments() map[string]string
}

// DefaultPayload is used for whenever no special case is needed
type DefaultPayload struct {
	Secret     string            `json:"secret"`
	ShouldWait bool              `json:"wait-for-execution"`
	Arguments  map[string]string `json:"arguments"`
}

// GetSecret implementation for DefaultPayload
func (p *DefaultPayload) GetSecret() string {
	return p.Secret
}

// ShouldWaitForExecution implementation for DefaultPayloads
func (p *DefaultPayload) ShouldWaitForExecution() bool {
	return p.ShouldWait
}

// GetArguments implementation for DefaultPayloads
func (p *DefaultPayload) GetArguments() map[string]string {
	return p.Arguments
}

// MakeDefaultPayloadFromBody creates a DefaultPayload struct from passed content
func MakeDefaultPayloadFromBody(r io.Reader) (*DefaultPayload, error) {
	var err error
	var b []byte
	p := DefaultPayload{}
	b, err = ioutil.ReadAll(r)
	if nil == err {
		err = json.Unmarshal(b, &p)
	}
	return &p, err
}

// CreateTokenPayload represents the body of an incoming
// HTTP request that want to create a new token
type CreateTokenPayload struct {
	Name           string   `json:"name"`
	AllowedOrigins []string `json:"allowed-origins"`
}
