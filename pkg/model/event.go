package model

// EventType describes which kind of event something is
type EventType int

const (
	// WebhookEvent for webhook triggering events
	WebhookEvent = iota
)

// Event ...
type Event struct {
	Type EventType
}
