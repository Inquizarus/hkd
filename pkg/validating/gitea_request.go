package validating

import (
	"encoding/json"
	"gitlab.com/inquizarus/hkd/pkg/storage"
)

// GiteaRequestAllowedToExecuteWebhook ...
func GiteaRequestAllowedToExecuteWebhook(bs []byte, s storage.ReadWriter) bool {
	payload := struct {
		Secret string `json:"secret"`
	}{}
	err := json.Unmarshal(bs, &payload)
	if nil != err {
		return false
	}
	return validEncodedTokenString(payload.Secret, s)
}
