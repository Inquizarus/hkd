package validating

import (
	"gitlab.com/inquizarus/hkd/pkg/storage"
	"net/http"
)

// GitlabRequestAllowedToExecuteWebhook ...
func GitlabRequestAllowedToExecuteWebhook(r *http.Request, s storage.Reader) bool {
	return validEncodedTokenString(r.Header.Get("X-Gitlab-Token"), s)
}
