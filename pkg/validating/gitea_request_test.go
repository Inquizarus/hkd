package validating_test

import (
	"fmt"
	"gitlab.com/inquizarus/hkd/pkg/mocking"
	"gitlab.com/inquizarus/hkd/pkg/validating"
	"testing"
)

func TestThatGiteaRequestWithoutTokenReturnsFalse(t *testing.T) {
	actual := validating.GiteaRequestAllowedToExecuteWebhook([]byte(""), mocking.Storage)
	expected := false
	if expected != actual {
		t.Error("true was returned for gitea request when false was expected")
	}
}

func TestThatGiteaRequestWithInvalidTokenReturnsFalse(t *testing.T) {
	actual := validating.GiteaRequestAllowedToExecuteWebhook([]byte(fmt.Sprintf(`{"secret": "%s"}`, mocking.EncodedInvalidTokenIdentifier)), mocking.Storage)
	expected := false
	if expected != actual {
		t.Error("true was returned for gitea request when false was expected")
	}
}

func TestThatGiteaRequestWithBadTokenEncodingReturnsFalse(t *testing.T) {
	actual := validating.GiteaRequestAllowedToExecuteWebhook([]byte(`{"secret": ")("/#(")#&&&¤¤}`), mocking.Storage)
	expected := false
	if expected != actual {
		t.Error("true was returned for gitea request when false was expected")
	}
}

func TestThatGiteaRequestWithTokenReturnsTrue(t *testing.T) {
	actual := validating.GiteaRequestAllowedToExecuteWebhook([]byte(fmt.Sprintf(`{"secret": "%s"}`, mocking.EncodedTokenIdentifier)), mocking.Storage)
	expected := true
	if expected != actual {
		t.Error("false was returned for gitea request when true was expected")
	}
}
