package validating

import (
	"gitlab.com/inquizarus/hkd/pkg/storage"
	"net/http"
	"strings"
)

// RequestAllowedToExecuteWebhook validates information inside request
// and returns true if webhook execution is allowed
func RequestAllowedToExecuteWebhook(r *http.Request, s storage.Reader) bool {
	auth := strings.SplitN(getAuthorizationTokenFromRequest(r), " ", 2)
	if len(auth) != 2 || auth[0] != "Bearer" {
		return false
	}
	return validEncodedTokenString(auth[1], s)
}

func getAuthorizationTokenFromRequest(r *http.Request) string {
	return r.Header.Get("Authorization")
}
