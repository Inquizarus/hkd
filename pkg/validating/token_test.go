package validating_test

import (
	"gitlab.com/inquizarus/hkd/pkg/mocking"
	"gitlab.com/inquizarus/hkd/pkg/model"
	ts "gitlab.com/inquizarus/hkd/pkg/storage/token"
	"gitlab.com/inquizarus/hkd/pkg/validating"
	"testing"
)

func TestThatTokenReturnTrueIfTokenIsValid(t *testing.T) {
	token := model.Token{
		Name:   "valid",
		Prefix: mocking.TokenIdentifierPrefix,
		Hash:   string(mocking.ValidTokenHash),
	}
	if true != validating.Token(token, mocking.Storage.TokenReadWriter()) {
		t.Error("valid token was evaluated to being invalid")
	}
}

func TestThatTokenReturnFalseIfTokenIsInvalid(t *testing.T) {
	trw := ts.MockReadWriter{}
	token := model.Token{}
	if true == validating.Token(token, &trw) {
		t.Errorf("invalid token was evaluated to being valid")
	}
}
