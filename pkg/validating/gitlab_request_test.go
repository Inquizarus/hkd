package validating_test

import (
	"bytes"
	"gitlab.com/inquizarus/hkd/pkg/mocking"
	"gitlab.com/inquizarus/hkd/pkg/validating"
	"net/http/httptest"
	"testing"
)

func TestThatGitlabRequestWithoutTokenReturnsFalse(t *testing.T) {
	r := httptest.NewRequest("POST", "/", bytes.NewReader([]byte("")))
	actual := validating.GitlabRequestAllowedToExecuteWebhook(r, mocking.Storage)
	expected := false
	if expected != actual {
		t.Error("true was returned for gitlab request when false was expected")
	}
}

func TestThatGitlabRequestWithInvalidTokenReturnsFalse(t *testing.T) {
	r := httptest.NewRequest("POST", "/", bytes.NewReader([]byte("")))
	r.Header.Set("X-Gitlab-Token", mocking.EncodedInvalidTokenIdentifier)
	actual := validating.GitlabRequestAllowedToExecuteWebhook(r, mocking.Storage)
	expected := false
	if expected != actual {
		t.Error("true was returned for gitlab request when false was expected")
	}
}

func TestThatGitlabRequestWithBadTokenEncodingReturnsFalse(t *testing.T) {
	r := httptest.NewRequest("POST", "/", bytes.NewReader([]byte("")))
	r.Header.Set("X-Gitlab-Token", ")(/#/¤(&#&")
	actual := validating.GitlabRequestAllowedToExecuteWebhook(r, mocking.Storage)
	expected := false
	if expected != actual {
		t.Error("true was returned for gitlab request when false was expected")
	}
}

func TestThatGitlabRequestWithTokenReturnsTrue(t *testing.T) {
	r := httptest.NewRequest("POST", "/", bytes.NewReader([]byte("")))
	r.Header.Set("X-Gitlab-Token", mocking.EncodedTokenIdentifier)
	actual := validating.GitlabRequestAllowedToExecuteWebhook(r, mocking.Storage)
	expected := true
	if expected != actual {
		t.Error("false was returned for gitlab request when true was expected")
	}
}
