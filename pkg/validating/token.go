package validating

import (
	"encoding/base64"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"gitlab.com/inquizarus/hkd/pkg/storage"
	ts "gitlab.com/inquizarus/hkd/pkg/storage/token"
)

func Token(t model.Token, trw ts.ReadWriter) bool {
	_, err := trw.Fetch(t.String())
	if nil != err {
		return false
	}
	return true
}

func validEncodedTokenString(t string, s storage.Reader) bool {
	if "" == t {
		return false
	}
	tb, err := base64.StdEncoding.DecodeString(t)
	if nil != err {
		return false
	}
	_, err = s.TokenReader().Fetch(string(tb))
	if err != nil {
		return false
	}
	return true
}
