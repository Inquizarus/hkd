package validating_test

import (
	"bytes"
	"fmt"
	"gitlab.com/inquizarus/hkd/pkg/mocking"
	"gitlab.com/inquizarus/hkd/pkg/validating"
	"net/http/httptest"
	"testing"
)

func TestThatGenericRequestIsAllowedToExecuteWebhook(t *testing.T) {
	t.Log("happy path for generic request validation for executing webhook")
	r := httptest.NewRequest("POST", "/webhook/some-webhook", bytes.NewBuffer([]byte("")))
	r.Header.Set("Authorization", fmt.Sprintf("Bearer %s", mocking.EncodedTokenIdentifier))
	expected := true
	if expected != validating.RequestAllowedToExecuteWebhook(r, mocking.Storage) {
		t.Errorf("RequestAllowedToExecuteWebhook returned %v when %v was expected", !expected, expected)
	}
}

func TestThatGenericRequestIsNotAllowedToExecuteWebhook(t *testing.T) {
	t.Log("sad path for generic request validation for executing webhook when token dont exist")
	r := httptest.NewRequest("POST", "/webhook/some-webhook", bytes.NewBuffer([]byte("")))
	r.Header.Set("Authorization", fmt.Sprintf("Bearer %s", mocking.EncodedInvalidTokenIdentifier))
	expected := false
	if expected != validating.RequestAllowedToExecuteWebhook(r, mocking.Storage) {
		t.Errorf("RequestAllowedToExecuteWebhook returned %v when %v was expected", !expected, expected)
	}
}

func TestThatGenericMalformedRequestIsNotAllowedToExecuteWebhook(t *testing.T) {
	t.Log("sad path for generic request validation for executing webhook when information is malformed")
	r := httptest.NewRequest("POST", "/webhook/some-webhook", bytes.NewBuffer([]byte("")))
	r.Header.Set("Authorization", fmt.Sprintf("%s", mocking.EncodedInvalidTokenIdentifier))
	expected := false
	if expected != validating.RequestAllowedToExecuteWebhook(r, mocking.Storage) {
		t.Errorf("RequestAllowedToExecuteWebhook returned %v when %v was expected", !expected, expected)
	}
}

func TestThatGenericRequestWithBadEncodingIsNotAllowedToExecuteWebhook(t *testing.T) {
	t.Log("sad path for generic request validation for executing webhook when information is malformed")
	r := httptest.NewRequest("POST", "/webhook/some-webhook", bytes.NewBuffer([]byte("")))
	r.Header.Set("Authorization", fmt.Sprintf("Bearer %s", "/#¤(/&#¤(/#&¤"))
	expected := false
	if expected != validating.RequestAllowedToExecuteWebhook(r, mocking.Storage) {
		t.Errorf("RequestAllowedToExecuteWebhook returned %v when %v was expected", !expected, expected)
	}
}
