package cmd

import (
	uuid "github.com/satori/go.uuid"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/inquizarus/hkd/pkg/config"
	"gitlab.com/inquizarus/hkd/pkg/generating"
	"gitlab.com/inquizarus/hkd/pkg/handling"
	"gitlab.com/inquizarus/rest"
)

func newServeCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {

	cmd := &cobra.Command{
		Use:   "serve",
		Short: "start built in server to expose rest api",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			port := viper.GetString("port")
			handlers := []rest.Handler{
				handling.MakeTokenHandler(
					config,
					generating.MakeRandomStringFunc(8, []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789")),
					func() string { return uuid.NewV4().String() },
				),
				handling.MakeSkeletonHandler(config),
				handling.MakeListHooksHandler(config),
				handling.MakeTriggerWebhookHandler(config),
			}
			c := rest.ServeConfig{
				Port:     port,
				Handlers: handlers,
			}
			rest.Serve(c)
		},
	}
	return cmd
}
