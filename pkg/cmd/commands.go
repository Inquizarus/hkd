package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/inquizarus/hkd/pkg/config"
)

func addCommands(cmd *cobra.Command, cfgBuilder config.BaseHkdConfigBuilder) {
	commands := []*cobra.Command{
		newServeCmd(cfgBuilder),
		newTokenCmd(cfgBuilder),
		newWebhookCmd(cfgBuilder),
		newVersionCmd(cfgBuilder),
	}
	cmd.AddCommand(commands...)
}
