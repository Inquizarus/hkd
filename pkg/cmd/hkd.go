package cmd

import (
	"fmt"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/inquizarus/hkd/pkg/config"
)

// Execute the application
func Execute(args []string, cfgBuilder config.BaseHkdConfigBuilder) {
	rootCmd := &cobra.Command{
		Use:   "hkd",
		Short: "hkd is a small webhook executing application",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
		},
	}
	initialize(rootCmd)
	addCommands(rootCmd, cfgBuilder)
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}

}

func initialize(cmd *cobra.Command) {
	cmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.hkd.json)")
	cmd.PersistentFlags().StringVarP(&port, "port", "p", "7411", "port number to use")
	cmd.PersistentFlags().StringP("data-dir", "D", "./data", "directory for persistance data")
	cmd.PersistentFlags().Bool("json", false, "output json instead of plain text")
	cmd.PersistentFlags().BoolP("hashing", "H", false, "setting to true enables hashing of vital information")
	cmd.PersistentFlags().String("hash-secret", "", "if non empty, hmac will be used for hashing instead of sha256")
	viper.BindPFlag("data-dir", cmd.PersistentFlags().Lookup("data-dir"))
	viper.BindPFlag("json", cmd.PersistentFlags().Lookup("json"))
	viper.BindPFlag("hash_secret", cmd.PersistentFlags().Lookup("hash-secret"))
	viper.BindPFlag("hashing", cmd.PersistentFlags().Lookup("hashing"))
	viper.BindPFlag("port", cmd.PersistentFlags().Lookup("port"))
}

func init() {
	cobra.OnInitialize(initConfig)
}

func initConfig() {
	viper.SetEnvPrefix("hkd")
	err := os.MkdirAll(viper.GetString("data-dir"), os.ModePerm)
	if nil != err {
		fmt.Println(err)
		os.Exit(1)
	}
	// Don't forget to read config either from cfgFile or from home directory!
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		viper.SetConfigType("json")
		// Search config in home directory with name ".cobra" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".hkd")
	}
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("Can't read config:", err)
		os.Exit(1)
	}
}
