package cmd

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/inquizarus/hkd/internal/executing"

	"github.com/spf13/cobra"
	"gitlab.com/inquizarus/hkd/pkg/config"
	"gitlab.com/inquizarus/hkd/pkg/generating"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"gitlab.com/inquizarus/hkd/pkg/util"
)

func newWebhookCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "webhook",
		Short: "command to handle webhook",
		Long:  ``,
		Run:   func(cmd *cobra.Command, args []string) {},
	}
	cmd.AddCommand(
		newWebhookListCmd(cfgBuilder),
		newWebhookCreateCmd(cfgBuilder),
		newWebhookShowCmd(cfgBuilder),
		newWebhookDeleteCmd(cfgBuilder),
		newWebhookPurgeCmd(cfgBuilder),
		newWebhookExecuteCmd(cfgBuilder),
		newWebhookSkeletonCmd(cfgBuilder),
	)
	return cmd
}

func newWebhookSkeletonCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	return &cobra.Command{
		Use:   "skeleton",
		Short: "generate and output a json skeleton for webhook",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			b, err := json.Marshal(generating.WebhookSkeleton())
			if nil != err {
				config.Logger().Error(err.Error())
				return
			}
			config.Printer().PrintText(string(b))
		},
	}
}

func newWebhookListCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "list all webhooks",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			whl, err := config.Storage().WebhookReader().FetchAll()
			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not list webhooks")
				os.Exit(1)
			}
			config.Printer().PrintWebhooks(whl)
		},
	}
}

func newWebhookShowCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "show",
		Short: "show a webhook",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			config.Logger().Debug("starting show webhook cmd")
			webhookIdentifier, err := cmd.Flags().GetString("webhook-identifier")

			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not show webhook")
				return
			}

			webhook, err := config.Storage().WebhookReader().Fetch(webhookIdentifier)

			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not show webhook")
				return
			}

			config.Printer().PrintWebhook(webhook)
		},
	}
	cmd.Flags().String("webhook-identifier", "", "webhook identifier")
	cmd.MarkFlagRequired("webhook-identifier")
	return cmd
}

func newWebhookCreateCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create",
		Short: "create a webhook",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			config.Logger().Debug("starting create webhook cmd")
			payload, err := cmd.Flags().GetString("payload")

			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not create webhook")
				return
			}

			config.Logger().Info(fmt.Sprintf("webhook payload \"%s\"", payload))

			wh := model.Webhook{}
			b := []byte(payload)
			err = json.Unmarshal(b, &wh)
			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not create webhook")
				return
			}

			err = config.Storage().WebhookWriter().Create(wh.Identifier, wh)

			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not create webhook")
				return
			}
		},
	}
	cmd.Flags().String("payload", "", "webhook content")
	cmd.MarkFlagRequired("payload")
	return cmd
}

func newWebhookDeleteCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "delete",
		Short: "delete specific webhook from storage",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			webhookIdentifier, _ := cmd.Flags().GetString("webhook-identifier")
			webhook, err := config.Storage().WebhookReader().Fetch(webhookIdentifier)
			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText(fmt.Sprintf("could not find webhook with identifier %s in storage", webhookIdentifier))
				return
			}
			forced, _ := cmd.Flags().GetBool("force")
			if false == forced {
				confirm := util.PromptConfirmation(fmt.Sprintf("This will remove webhook with identifier %s, are you sure?", webhookIdentifier), os.Stdin, os.Stdout)
				if false == confirm {
					config.Logger().Debug("user answered no to webhook purge confirmation")
					return
				}
			}
			err = config.Storage().WebhookReadWriter().Remove(webhook.Identifier)
			if nil != err {
				config.Logger().Error(err.Error())
				return
			}
			config.Printer().PrintText("webhook deleted")
		},
	}
	cmd.Flags().BoolP("force", "f", false, "force purge of webhooks")
	cmd.Flags().String("webhook-identifier", "", "webhook identifier")
	cmd.MarkFlagRequired("webhook-identifier")
	return cmd
}

func newWebhookPurgeCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "purge",
		Short: "purge webhooks from storage",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()

			hl, err := config.Storage().WebhookReader().FetchAll()
			if nil != err {
				config.Logger().Error(err.Error())
				return
			}
			forced, _ := cmd.Flags().GetBool("force")
			if false == forced {
				confirm := util.PromptConfirmation(fmt.Sprintf("This will remove all (%d) webhooks, are you sure?", len(hl)), os.Stdin, os.Stdout)
				if false == confirm {
					config.Logger().Debug("user answered no to token purge confirmation")
					return
				}
			}

			for _, hook := range hl {
				err := config.Storage().WebhookWriter().Remove(hook.Identifier)
				if nil != err {
					config.Logger().Error(err.Error())
					return
				}
			}
			config.Logger().Debug("webhooks purged successfully")
			config.Printer().PrintText("webhooks purged")
		},
	}
	cmd.Flags().BoolP("force", "f", false, "force purge of webhooks")
	return cmd
}

func newWebhookExecuteCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	identifierKey := "webhook-identifier"
	payloadKey := "payload"
	cmd := &cobra.Command{
		Use:   "execute",
		Short: "execute a webhook",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			cfg := cfgBuilder.Build()
			identifier, err := cmd.Flags().GetString(identifierKey)
			if nil != err {
				cfg.Logger().Error(err.Error())
				return
			}
			payloadString, err := cmd.Flags().GetString(payloadKey)
			if nil != err {
				cfg.Logger().Error(err.Error())
				return
			}
			payload := model.DefaultPayload{}
			json.Unmarshal([]byte(payloadString), &payload)
			wh, err := cfg.Storage().WebhookReader().Fetch(identifier)
			if nil != err {
				cfg.Printer().PrintText(fmt.Sprintf("could not find webhook with identifier %s", identifier))
				cfg.Logger().Error(err.Error())
				return
			}
			out, err := executing.Webhook(wh, &payload)
			if nil != err {
				cfg.Printer().PrintText("could not execute webhook")
				cfg.Logger().Error(err.Error())
				return
			}
			cfg.Printer().PrintText(out)
		},
	}
	cmd.Flags().String(identifierKey, "", "identifier of webhook to execute")
	cmd.Flags().String(payloadKey, "", "payload for webhook")
	cmd.MarkFlagRequired(identifierKey)
	cmd.MarkFlagRequired(payloadKey)
	return cmd
}
