package cmd

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/inquizarus/hkd/pkg/generating"
	"gitlab.com/inquizarus/hkd/pkg/util"

	"github.com/spf13/cobra"
	"gitlab.com/inquizarus/hkd/pkg/config"
	"gitlab.com/inquizarus/hkd/pkg/model"
)

func newTokenCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "token",
		Short: "token handling",
		Long:  ``,
		Run:   func(cmd *cobra.Command, args []string) {},
	}
	cmd.AddCommand(
		newTokenListCmd(cfgBuilder),
		newTokenCreateCmd(cfgBuilder),
		newTokenShowCmd(cfgBuilder),
		newTokenSkeletonCmd(cfgBuilder),
		newTokenPurgeCmd(cfgBuilder),
		newTokenDeleteCmd(cfgBuilder),
	)
	return cmd
}

func newTokenSkeletonCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	return &cobra.Command{
		Use:   "skeleton",
		Short: "generate and output a json skeleton for token",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			b, err := json.Marshal(generating.TokenSkeleton())
			if nil != err {
				config.Logger().Error(err.Error())
				return
			}
			config.Printer().PrintText(string(b))
		},
	}
}

func newTokenShowCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "show",
		Short: "show a token",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			config.Logger().Debug("starting show token cmd")
			tokenIdentifier, err := cmd.Flags().GetString("token-identifier")

			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not show token")
				return
			}

			token, err := config.Storage().TokenReader().Fetch(tokenIdentifier)

			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not show token")
				return
			}

			config.Printer().PrintToken(token)
		},
	}
	cmd.Flags().String("token-identifier", "", "token identifier")
	cmd.MarkFlagRequired("token-identifier")
	return cmd
}

func newTokenListCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "list all tokens",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			tl, err := config.Storage().TokenReader().FetchAll()
			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not list tokens")
				os.Exit(1)
			}
			config.Logger().Debug(fmt.Sprintf("found %d tokens in storage with fetchAll", len(tl)))
			config.Printer().PrintTokens(tl)
		},
	}
}

func newTokenCreateCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "create",
		Short: "create a token",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			payload, err := cmd.Flags().GetString("payload")

			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not create token")
				return
			}

			config.Logger().Info(fmt.Sprintf("token payload \"%s\"", payload))

			t := model.Token{}
			b := []byte(payload)
			err = json.Unmarshal(b, &t)

			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not decode token payload")
				return
			}

			existingToken, err := config.Storage().TokenReader().Fetch(t.String())

			if existingToken.String() == t.String() {
				msg := fmt.Sprintf("token with identifier %s already exist", t.String())
				config.Logger().Debug(msg)
				config.Printer().PrintText(msg)
				return
			}

			err = config.Storage().TokenWriter().Create(t.String(), t)

			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText("could not create token")
				return
			}
		},
	}
	cmd.Flags().String("payload", "", "token content")
	cmd.MarkFlagRequired("payload")
	return cmd
}

func newTokenDeleteCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "delete",
		Short: "delete specific token from storage",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()
			tokenIdentifier, _ := cmd.Flags().GetString("token-identifier")
			token, err := config.Storage().TokenReader().Fetch(tokenIdentifier)
			if nil != err {
				config.Logger().Error(err.Error())
				config.Printer().PrintText(fmt.Sprintf("could not find token with identifier %s in storage", tokenIdentifier))
				return
			}
			forced, _ := cmd.Flags().GetBool("force")
			if false == forced {
				confirm := util.PromptConfirmation(fmt.Sprintf("This will remove token with identifier %s, are you sure?", tokenIdentifier), os.Stdin, os.Stdout)
				if false == confirm {
					config.Logger().Debug("user answered no to token purge confirmation")
					return
				}
			}
			err = config.Storage().TokenWriter().Remove(token.String())
			if nil != err {
				config.Logger().Error(err.Error())
				return
			}
			config.Printer().PrintText("token deleted")
		},
	}
	cmd.Flags().BoolP("force", "f", false, "force purge of tokens")
	cmd.Flags().String("token-identifier", "", "token identifier")
	cmd.MarkFlagRequired("token-identifier")
	return cmd
}

func newTokenPurgeCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "purge",
		Short: "purge tokens from storage",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			config := cfgBuilder.Build()

			tl, err := config.Storage().TokenReader().FetchAll()
			if nil != err {
				config.Logger().Error(err.Error())
				return
			}
			forced, _ := cmd.Flags().GetBool("force")
			if false == forced {
				confirm := util.PromptConfirmation(fmt.Sprintf("This will remove all (%d) tokens, are you sure?", len(tl)), os.Stdin, os.Stdout)
				if false == confirm {
					config.Logger().Debug("user answered no to token purge confirmation")
					return
				}
			}
			for _, token := range tl {
				err := config.Storage().TokenWriter().Remove(token.String())
				if nil != err {
					config.Logger().Error(err.Error())
					return
				}
			}
			config.Logger().Debug("tokens purged successfully")
			config.Printer().PrintText("tokens purged")
		},
	}
	cmd.Flags().BoolP("force", "f", false, "force purge of tokens")
	return cmd
}
