package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/inquizarus/hkd/pkg/config"
	"gitlab.com/inquizarus/hkd/pkg/util"
)

func newVersionCmd(cfgBuilder config.BaseHkdConfigBuilder) *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "command to see hkd version",
		Long:  ``,
		Run: func(cmd *cobra.Command, args []string) {
			cfg := cfgBuilder.Build()
			version := util.Version{}
			cfg.Printer().PrintText(version.String())
		},
	}
}
