package mocking

import (
	"errors"
	"fmt"
	"gitlab.com/inquizarus/hkd/pkg/model"
	"gitlab.com/inquizarus/hkd/pkg/storage"
)

// Storage ...
var Storage = storage.MakeMockReadWriter(
	map[string]model.Token{
		ValidTokenIdentifier: model.Token{
			Name:   "valid",
			Prefix: TokenIdentifierPrefix,
			Hash:   string(ValidTokenHash),
		},
	},
	map[string]error{
		fmt.Sprintf("%s.%s", TokenIdentifierPrefix, HashedInsertErrorTokenIdentifier):   errors.New("token insertion error"),
		fmt.Sprintf("%s.%s", TokenIdentifierPrefix, HashedInsertSuccessTokenIdentifier): nil,
	},
	map[string]model.Webhook{
		ValidWebhookIdentifier: model.Webhook{
			Identifier:       ValidWebhookIdentifier,
			WorkingDirectory: "./",
			ExecuteCommand:   "/bin/echo",
			CommandArgs:      []string{`"testing with echo"`},
		},
		ValidFailingWebhookIdentifier: model.Webhook{
			Identifier:       ValidFailingWebhookIdentifier,
			WorkingDirectory: "./",
			ExecuteCommand:   "/bin/non-existend-command",
		},
	},
)
