package mocking

import (
	"gitlab.com/inquizarus/hkd/pkg/hashing"
)

// MakeHasher returns a mocked hasher func where input is compared to passed
// map and returns found value
func MakeHasher(hm map[string][]byte) hashing.Hasher {
	return func(input []byte) []byte {
		return hm[string(input)]
	}
}

// MockHasher which uses a static secret
var MockHasher = hashing.MakeHmac([]byte("mocking"))
