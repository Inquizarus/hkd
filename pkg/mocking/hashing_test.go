package mocking_test

import (
	"gitlab.com/inquizarus/hkd/pkg/mocking"
	"gitlab.com/inquizarus/hkd/pkg/util"
	"testing"
)

func TestThatMockHasherWorks(t *testing.T) {
	validInput := []byte("valid input")
	validOutput := []byte("valid output")
	hm := map[string][]byte{
		string(validInput): validOutput,
		"":                 []byte{},
	}
	hasher := mocking.MakeHasher(hm)

	cases := map[string][]byte{
		string(validInput): validOutput,
		"":                 []byte{},
	}
	for k, v := range cases {
		actual := hasher([]byte(k))
		if true != util.EqualByteSlices(v, actual) {
			t.Errorf("mock hasher returned wrong value, expected %v but got %v", v, actual)
		}
	}
}
