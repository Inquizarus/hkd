package mocking

import (
	"encoding/base64"
	"fmt"
)

// TokenIdentifierPrefix ...
var TokenIdentifierPrefix = "abc"

// TokenIdentifierKey ...
var TokenIdentifierKey = "xyz"

// InsertSuccessTokenIdentifierKey ...
var InsertSuccessTokenIdentifierKey = fmt.Sprintf("%s-%s", TokenIdentifierKey, "insert-success")

// InsertErrorTokenIdentifierKey ...
var InsertErrorTokenIdentifierKey = fmt.Sprintf("%s-%s", TokenIdentifierKey, "insert-error")

// ValidTokenKey ...
var ValidTokenKey = fmt.Sprintf("%s.%s", TokenIdentifierPrefix, TokenIdentifierKey)

// ValidTokenHash ...
var ValidTokenHash = MockHasher([]byte(ValidTokenKey))

// ValidTokenIdentifier ...
var ValidTokenIdentifier = fmt.Sprintf("%s.%s", TokenIdentifierPrefix, ValidTokenHash)

// InsertSuccessTokenIdentifier for when you need a token insertion success result
var InsertSuccessTokenIdentifier = fmt.Sprintf("%s.%s", TokenIdentifierPrefix, InsertSuccessTokenIdentifierKey)

// HashedInsertSuccessTokenIdentifier is what the can says
var HashedInsertSuccessTokenIdentifier = MockHasher([]byte(InsertSuccessTokenIdentifier))

// HashedInsertErrorTokenIdentifier is what the can says
var HashedInsertErrorTokenIdentifier = MockHasher([]byte(InsertErrorTokenIdentifier))

// InsertErrorTokenIdentifier for when you need a token insertion error result
var InsertErrorTokenIdentifier = fmt.Sprintf("%s.%s", TokenIdentifierPrefix, InsertErrorTokenIdentifierKey)

// InvalidTokenIdentifier ...
var InvalidTokenIdentifier = "xyz"

// EncodedTokenIdentifier ...
var EncodedTokenIdentifier = base64.StdEncoding.EncodeToString([]byte(ValidTokenIdentifier))

// EncodedInvalidTokenIdentifier ...
var EncodedInvalidTokenIdentifier = base64.StdEncoding.EncodeToString([]byte(InvalidTokenIdentifier))
