package mocking

import (
	"encoding/base64"
	"fmt"
)

// ValidWebhookIdentifier ...
var ValidWebhookIdentifier = "valid"

// EncodedValidWebhookIdentifier ...
var EncodedValidWebhookIdentifier = base64.StdEncoding.EncodeToString([]byte(ValidWebhookIdentifier))

// ValidFailingWebhookIdentifier ...
var ValidFailingWebhookIdentifier = "valid-failing"

// EncodedValidFailingWebhookIdentifier ...
var EncodedValidFailingWebhookIdentifier = base64.StdEncoding.EncodeToString([]byte(ValidFailingWebhookIdentifier))

// InvalidWebhookIdentifier ...
var InvalidWebhookIdentifier = "invalidvalid"

// EncodedInvalidWebhookIdentifier ...
var EncodedInvalidWebhookIdentifier = base64.StdEncoding.EncodeToString([]byte(InvalidWebhookIdentifier))

var webhookURLPattern = "/webhook/%s"

// ExistingWebhookURL ...
var ExistingWebhookURL = fmt.Sprintf(webhookURLPattern, EncodedValidWebhookIdentifier)

// ExistingFailingWebhookURL ...
var ExistingFailingWebhookURL = fmt.Sprintf(webhookURLPattern, EncodedValidFailingWebhookIdentifier)

// NonExistingWebhookURL ...
var NonExistingWebhookURL = fmt.Sprintf(webhookURLPattern, EncodedInvalidWebhookIdentifier)
