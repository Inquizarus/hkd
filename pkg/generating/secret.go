package generating

import (
	"math/rand"
	"strings"
	"time"
)

// String randomly creates a string to use as a secret
func String(l int) string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZÅÄÖ" +
		"abcdefghijklmnopqrstuvwxyzåäö" +
		"0123456789" +
		"!@#")
	b := strings.Builder{}
	for index := 0; index < l; index++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	return b.String()
}
