package generating_test

import (
	"gitlab.com/inquizarus/hkd/pkg/generating"
	"testing"
)

func TestThatRandomStringFuncWorksCorrectly(t *testing.T) {
	cases := []struct {
		E string
		L int
		C []rune
	}{
		{
			E: "aaaaa",
			L: 5,
			C: []rune("a"),
		},
		{
			E: "bbb",
			L: 3,
			C: []rune("b"),
		},
		{
			E: "!!!!",
			L: 4,
			C: []rune("!"),
		},
	}
	for _, c := range cases {
		f := generating.MakeRandomStringFunc(c.L, c.C)
		actual := f()
		if len(c.E) != len(actual) {
			t.Errorf("wrong length on random string, expected %d but got %d", len(c.E), len(actual))
		}
		if c.E != actual {
			t.Errorf("wrong random string generated, expected %s but got %s", c.E, actual)
		}
	}
}
