package generating

import (
	"math/rand"
	"strings"
	"time"
)

// MakeRandomStringFunc currys the returned function with passed
// parameters to generate a fixed length string out of the rune set
func MakeRandomStringFunc(l int, chars []rune) func() string {
	return func() string {
		rand.Seed(time.Now().UnixNano())
		b := strings.Builder{}
		for index := 0; index < l; index++ {
			b.WriteRune(chars[rand.Intn(len(chars))])
		}
		return b.String()
	}
}
