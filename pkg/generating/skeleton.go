package generating

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/inquizarus/hkd/pkg/model"
)

// TokenSkeleton returns a Token model prepared for usage
// an uuid identifier is already generated
func TokenSkeleton() model.Token {
	return model.Token{
		AllowedOrigins: []string{"*"},
	}
}

// WebhookSkeleton returns a Hook model prepared for usage
// an uuid identifier is already generated
func WebhookSkeleton() model.Webhook {
	return model.Webhook{
		Identifier:       uuid.NewV4().String(),
		AllowedOrigins:   []string{"*"},
		CommandArgs:      []string{},
		AllowedArguments: []string{},
	}
}
