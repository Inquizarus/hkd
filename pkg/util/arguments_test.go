package util_test

import (
	"testing"

	"gitlab.com/inquizarus/hkd/pkg/util"
)

func TestBuildArgumentList(t *testing.T) {
	input := map[string]string{
		"project":   "bananacake",
		"--verbose": "true",
	}
	expected := []string{"bananacake", "--verbose true"}
	actual := util.BuildArgumentList(input)

	if len(actual) != len(expected) {
		t.Errorf("wrong size of returned slice, expected %d but received %d", len(expected), len(actual))
	}

	for index := 0; index < len(expected); index++ {
		if ok := actual[index]; ok == "" {
			t.Errorf("expected argument %s not in actual result", expected[index])
		}
	}

}
