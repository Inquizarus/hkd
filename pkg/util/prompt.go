package util

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"strings"
)

// PromptConfirmation asks the user to confirm something and returns true if so else false
func PromptConfirmation(msg string, r io.Reader, w io.Writer) bool {
	reader := bufio.NewReader(r)
	for {
		fmt.Fprintf(w, "%s [y/n]: ", msg)
		response, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}
		response = strings.ToLower(strings.TrimSpace(response))
		if response == "y" || response == "yes" {
			return true
		} else if response == "n" || response == "no" {
			return false
		}
		return false
	}
}
