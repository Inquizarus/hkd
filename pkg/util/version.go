package util

// Version represents the current hkd version
type Version struct{}

func (v *Version) String() string {
	return "0.1.0"
}

// Bytes returns slice of bytes representing current version
func (v *Version) Bytes() []byte {
	return []byte(v.String())
}
