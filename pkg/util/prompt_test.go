package util_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"testing"

	"gitlab.com/inquizarus/hkd/pkg/util"
)

func TestThatPromptConfirmationAsksCorrectly(t *testing.T) {
	msg := "are you sure?"
	reader := &bytes.Buffer{}
	writer := &bytes.Buffer{}
	reader.Write([]byte("yes\n"))
	util.PromptConfirmation(msg, reader, writer)
	wc, _ := ioutil.ReadAll(writer)
	actual := string(wc)
	expected := fmt.Sprintf("%s [y/n]: ", msg)
	if expected != actual {
		t.Errorf("prompt for confirmation did not ask correctly, got \"%s\" but wanted \"%s\"", actual, expected)
	}
}

func TestThatPromptConfirmationReturnsTrueWhenConfirmed(t *testing.T) {
	reader := &bytes.Buffer{}
	writer := &bytes.Buffer{}
	reader.Write([]byte("yes\n"))
	actual := util.PromptConfirmation("some confirmation question?", reader, writer)
	expected := true
	if expected != actual {
		t.Errorf("prompt for confirmation return correct value when confirming, got \"%v\" but wanted \"%v\"", actual, expected)
	}
}

func TestThatPromptConfirmationReturnsFalseWhenNotConfirmed(t *testing.T) {
	reader := &bytes.Buffer{}
	writer := &bytes.Buffer{}
	reader.Write([]byte("no\n"))
	actual := util.PromptConfirmation("some confirmation question?", reader, writer)
	expected := false
	if expected != actual {
		t.Errorf("prompt for confirmation return correct value when confirming, got \"%v\" but wanted \"%v\"", actual, expected)
	}
}

func TestThatPromptConfirmationReturnsFalseWhenInvalidAnswerIsUsed(t *testing.T) {
	reader := &bytes.Buffer{}
	writer := &bytes.Buffer{}
	reader.Write([]byte("9nlkj324klj234lkj23477!!¤\n"))
	actual := util.PromptConfirmation("some confirmation question?", reader, writer)
	expected := false
	if expected != actual {
		t.Errorf("prompt for confirmation return correct value when confirming, got \"%v\" but wanted \"%v\"", actual, expected)
	}
}
