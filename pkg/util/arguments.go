package util

import (
	"fmt"
	"strings"
)

// BuildArgumentList transforms a map into an array of combined arguments
// where if a key have a dash prefix it becomes a --k v pair and otherwise only
// the v is entered into the returned list
func BuildArgumentList(args map[string]string) []string {
	var argList []string
	for k, v := range args {
		if strings.HasPrefix(k, "-") {
			argList = append(argList, fmt.Sprintf("%s %s", k, v))
			continue
		}
		argList = append(argList, v)
	}
	return argList
}
