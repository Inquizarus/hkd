package util

// EqualByteSlices compares two slices of bytes and returns true if same
func EqualByteSlices(a, b []byte) bool {

	if (nil == a) != (nil == b) {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}
