package util_test

import (
	"gitlab.com/inquizarus/hkd/pkg/util"
	"testing"
)

func TestThatEqualByteSlicesWorkAsIntended(t *testing.T) {
	errorPattern := "comparison of byte slices in case %d got unexpected result, got %v but wanted %v"
	cases := []struct {
		a []byte
		b []byte
		e bool
	}{
		{
			a: []byte("abcd"),
			b: []byte("abcd"),
			e: true,
		},
		{
			a: []byte("abcd"),
			b: []byte("dcba"),
			e: false,
		},
		{
			a: []byte("abcd"),
			b: []byte("abc"),
			e: false,
		},
		{
			a: nil,
			b: nil,
			e: true,
		},
		{
			a: []byte("abcd"),
			b: nil,
			e: false,
		},
	}
	for k, c := range cases {
		if c.e != util.EqualByteSlices(c.a, c.b) {
			t.Errorf(errorPattern, k, !c.e, c.e)
		}
	}
}
