package util_test

import (
	"gitlab.com/inquizarus/hkd/pkg/util"
	"testing"
)

func TestThatCorrectVersionStringIsReturned(t *testing.T) {
	expected := "0.1.0"
	version := util.Version{}
	if expected != version.String() {
		t.Errorf("incorrect version string, got %s but expected %s", version, expected)
	}
}

func TestThatCorrectVersionBytesIsReturned(t *testing.T) {
	expected := []byte("0.1.0")
	version := util.Version{}
	if true != util.EqualByteSlices(expected, version.Bytes()) {
		t.Errorf("incorrect version bytes, got %v but expected %v", version.Bytes(), expected)
	}
}
