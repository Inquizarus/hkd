package config

import (
	"gitlab.com/inquizarus/hkd/pkg/hashing"
	"io"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/inquizarus/hkd/pkg/logging"
	"gitlab.com/inquizarus/hkd/pkg/printing"
	"gitlab.com/inquizarus/hkd/pkg/storage"
)

// BaseHkdConfigBuilder interface
type BaseHkdConfigBuilder interface {
	Build() HkdConfig
}

// HkdConfigBuilder interface
type HkdConfigBuilder interface {
	BaseHkdConfigBuilder
	WithLogger(logging.Logger)
	WithPrinter(printing.Printer)
	WithStorage(storage.ReadWriter)
	Wrap() BaseHkdConfigBuilder
}

// HkdConfigBuilderWrapper implementation of BaseHkdConfigBuilder
type HkdConfigBuilderWrapper struct {
	b BaseHkdConfigBuilder
}

// Build implementation for HkdConfigBuilderWrapper
func (bw *HkdConfigBuilderWrapper) Build() HkdConfig {
	return bw.b.Build()
}

// StandardHkdConfigBuilder implementation of HkdConfigBuilder interface
type StandardHkdConfigBuilder struct {
	printer printing.Printer
	logger  logging.Logger
	storage storage.ReadWriter
	hasher  hashing.Hasher
}

// Build implementation for StandardHkdConfigBuilder
func (sb *StandardHkdConfigBuilder) Build() HkdConfig {
	if nil == sb.printer {
		sb.printer = getPrinter(os.Stdout)
	}
	if nil == sb.logger {
		sb.logger = logging.MakeDefaultLogger()
	}
	if nil == sb.storage {
		sb.storage = storage.MakeStandardReadWriter()
	}
	return &StandardHkdConfig{
		logger:  sb.logger,
		printer: sb.printer,
		storage: sb.storage,
		hasher:  sb.hasher,
	}
}

// WithLogger implementation for StandardHkdConfigBuilder
func (sb *StandardHkdConfigBuilder) WithLogger(l logging.Logger) {
	sb.logger = l
}

// WithPrinter implementation for StandardHkdConfigBuilder
func (sb *StandardHkdConfigBuilder) WithPrinter(p printing.Printer) {
	sb.printer = p
}

// WithStorage implementation for StandardHkdConfigBuilder
func (sb *StandardHkdConfigBuilder) WithStorage(s storage.ReadWriter) {
	sb.storage = s
}

// WithHasher implementation for StandardHkdConfigBuilder
func (sb *StandardHkdConfigBuilder) WithHasher(h hashing.Hasher) {
	sb.hasher = h
}

// Wrap implementation for StandardHkdConfigBuilder
func (sb *StandardHkdConfigBuilder) Wrap() BaseHkdConfigBuilder {
	return &HkdConfigBuilderWrapper{
		b: sb,
	}
}

func getPrinter(w io.Writer) printing.Printer {
	if true == viper.GetBool("json") {
		return printing.MakeJSONPrinter(w)
	}
	return printing.MakeDefaultPrinter(w)
}
