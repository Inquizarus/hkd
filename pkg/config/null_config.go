package config

import (
	"gitlab.com/inquizarus/hkd/pkg/logging"
	"gitlab.com/inquizarus/hkd/pkg/printing"
	"gitlab.com/inquizarus/hkd/pkg/storage"
)

// NullHkdConfig implementation of HkdConfig interface
type NullHkdConfig struct{}

// Logger implementation for HkdConfig interface
func (scfg *NullHkdConfig) Logger() logging.Logger {
	return &logging.NullLogger{}
}

// Printer implementation for HkdConfig interface
func (scfg *NullHkdConfig) Printer() printing.Printer {
	return &printing.NullPrinter{}
}

// Storage implementation for HkdConfig interface
func (scfg *NullHkdConfig) Storage() storage.ReadWriter {
	return &storage.NullReadWriter{}
}

// Hash implementation for HkdConfig interface
func (scfg *NullHkdConfig) Hash(input string) string {
	return ""
}
