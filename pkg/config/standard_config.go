package config

import (
	"gitlab.com/inquizarus/hkd/pkg/hashing"
	"gitlab.com/inquizarus/hkd/pkg/logging"
	"gitlab.com/inquizarus/hkd/pkg/printing"
	"gitlab.com/inquizarus/hkd/pkg/storage"
)

// StandardHkdConfig implementation of HkdConfig interface
type StandardHkdConfig struct {
	printer printing.Printer
	logger  logging.Logger
	storage storage.ReadWriter
	hasher  hashing.Hasher
}

// Logger implementation for HkdConfig interface
func (scfg *StandardHkdConfig) Logger() logging.Logger {
	return scfg.logger
}

// Printer implementation for HkdConfig interface
func (scfg *StandardHkdConfig) Printer() printing.Printer {
	return scfg.printer
}

// Storage implementation for HkdConfig interface
func (scfg *StandardHkdConfig) Storage() storage.ReadWriter {
	return scfg.storage
}

// Hash implementation for HkdConfig interface
func (scfg *StandardHkdConfig) Hash(input string) string {
	return string(scfg.hasher([]byte(input)))
}
