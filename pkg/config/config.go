package config

import (
	"gitlab.com/inquizarus/hkd/pkg/logging"
	"gitlab.com/inquizarus/hkd/pkg/printing"
	"gitlab.com/inquizarus/hkd/pkg/storage"
)

// HkdConfig configuration interface
type HkdConfig interface {
	Logger() logging.Logger
	Printer() printing.Printer
	Storage() storage.ReadWriter
	Hash(string) string
}
